# Nástroje pro tvorbu, správu a nasazování aplikací

POZOR! jedná se pouze o **pracovní verzi** skript!

První oficiální vydání bude k dispozici na konci roku 2019.

Živá HTML verze (v budoucnu v lepší šabloně) skript se nachází na:

https://fis-vse.gitlab.io/studijni-materialy/softwarove-inzenyrstvi

Připomínky je možné zadat s sekci [issues](https://gitlab.com/FIS-VSE/studijni-materialy/softwarove-inzenyrstvi/issues).

## Obsah

1. [Úvod](src/Uvod.md)
1. [Java](src/Java.md)
1. [JavaFX](src/JavaFx.md)
1. [Vývojové prostředí](src/IDE.md)
1. [Správa verzí](src/Git.md)
1. [Správa projektu](src/Maven.md)
1. [Založení JavaFX projektu](src/ZalozeniFxProjektu.md)
1. [Sdílení projektu](src/SdileniProjektu.md)
1. [Klonování projektu](src/KlonovaniProjektu.md)
1. [Architektura JavaFX projektu](src/ArchitekturaFxProjektu.md)
1. [Sestavení projektu](src/SestaveniFxProjektu.md)
1. [Týmová práce](src/TymovaPrace.md)
1. [Release aplikace](src/Release.md)
