#!/bin/bash

# odstranění složky s výstupy
rm -rf public
mkdir public

# sjednocení kapitol do knihy
cat \
src/meta.md \
src/01_Uvod.md  \
src/02_Java.md \
src/03_JavaFx.md \
src/04_IDE.md \
src/05_Git.md \
src/06_Maven.md \
src/07_ZalozeniFxProjektu.md \
src/07_ZalozeniFxProjektuA.md \
src/07_ZalozeniFxProjektuB.md \
src/07_ZalozeniFxProjektuC.md \
src/07_ZalozeniFxProjektuD.md \
src/07_ZalozeniFxProjektuE.md \
src/08_SdileniProjektu.md \
src/09_KlonovaniProjektu.md \
src/10_ArchitekturaFxProjektu.md \
src/11_SestaveniFxProjektu.md \
src/12_TymovaPrace.md \
src/13_Release.md \
> public/source.md

# překopírování obrázků a souborů webové šablony
cp -r src/images public/images
cp -r template/web-assets/assets public/assets
cp -r template/web-assets/images/* public/images/

cd public

# vytvoření verzí v různých formátech

pandoc source.md -N \
--include-before-body=../src/Preface.md \
--dpi=300 \
--template=../template/template.latex \
--highlight-style=zenburn \
--output ebook_4IT115_2019_SNAPSHOT.pdf

pandoc source.md -t gfm+gfm_auto_identifiers -o ebook_4IT115_2019_SNAPSHOT.md

pandoc source.md \
--section-divs \
--base-header-level=2 \
--toc \
--toc-depth=2 \
--write=html5 \
--highlight-style=zenburn \
--template=../template/template.html \
--output=index.html

#pandoc source.md -o book.docx
