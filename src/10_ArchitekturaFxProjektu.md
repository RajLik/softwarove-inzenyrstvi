
# Architektura JavaFX projektu {#architekturaProjektu}

Kapitola popisuje architektonické volby pro novou JavaFX aplikaci a představuje minimální architektonický návrh nutný pro vykreslení okna a přípravu na grafické prvky a ovládání jejich událostí.

Při návrhu architektury je výhodné aplikaci rozdělit do menších celků, které usnadní její správu a umožní lepší spolupráci v týmu vývojářů. Způsobů, jak aplikaci rozdělit, je mnoho. Obecně se jako dobrá praxe vžilo minimálně **oddělení uživatelského rozhraní, byznys logiky a dat**, které reprezentují architektonické vzory MVx: [Model-View-Controller (MVC)](https://en.wikipedia.org/wiki/Model-view-controller), [Model-View-Presenter (MVP)](https://en.wikipedia.org/wiki/Model-view-presenter), [Model-View-ViewModel (MVVM)](https://en.wikipedia.org/wiki/Model-view-viewmodel). Jaký návrhový vzor a jaká implementace vzoru je pro JavaFX aplikace nejvhodnější?

Kritéria pro volbu architektury:

* srozumitelnost kódu,
* snadné testování,
* snazší rozdělení týmových rolí,
* minimalizace duplicitního kódu (boilerplate).

Implementace architektury se liší zejména tím, jak jsou jednotlivé části vzájemně propojené a jak spolupracují. Rozdíly velmi dobře shrnuje článek [Model-View-Whatever](https://www.beyondjava.net/model-view-whatever) na BeyondJava.

Princip návrhového vzoru **Model-View-Controller** (MVC) spočívá v tom, že *view* reaguje na vstupy od uživatele a posílá o nich zprávy kontroleru (*controller*), který upravuje *model*. Model potom posílá informace o změnách *view*.

![Model-View-Controller (zdroj: Stephan Rauth)](https://www.beyondjava.net/blog/images/mvw/MVC.png){ height=5cm }

**Model-View-Presenter** (MVP) provázanost snižuje a namísto kontroleru, který by jen zachytával vstupy uživatele, představuje prezentér (*presenter*). Prezentér funguje jako mezivrstva. Je odpovědný za komunikaci mezi *view* a modelem.

![Model-View-Presenter (zdroj: Stephan Rauth)](https://www.beyondjava.net/blog/images/mvw/MVP.png){ height=5cm }

**Model-View-ViewModel** (MVVM) využívá v komunikaci mezi view a prostředníkem *viewmodel* princip [*data binding*](https://en.wikipedia.org/wiki/Data_binding), který se hojně používá při vývoji aplikací pro Android. Data binding si lze zjednodušeně představit jako propojení na bázi synchronizace.

![Model-View-ViewModel (zdroj: Stephan Rauth)](https://www.beyondjava.net/blog/images/mvw/MVVM.png){ height=5cm }

## Volba implementace MVx

Jednou z výhod, kterou má JavaFX oproti starším grafickým knihovnám, je možnost použití **deklarativního zápisu** grafického rozhraní formou FXML.

Výhody použití deklarativního zápisu:

* možnost využít aplikaci pro usnadnění návrhu (např. Scene Builder),
* rychlejší tvorba rozhraní pro účely uživatelského testování,
* návrh UI je možné uskutečnit bez znalosti programování.

Při tvorbě JavaFX aplikace za pomoci FXML by se mohlo zdát, že už implementace směřuje určitým směrem. JavaFX knihovna dokáže propojit jeden FXML soubor s právě jedním `fx:controller`. Ten není definován jako potomek žádné knihovní třídy, takže je na vývojáři, jak třídu deklaruje, ale správně by měl být potomkem kořenového prvku FXML (například *GridPane*). `fx:controller` má za [cíl](https://docs.oracle.com/javafx/2/api/javafx/fxml/doc-files/introduction_to_fxml.html#controllers) řešit přístup k jednotlivým ovládacím prvkům a řešit reakce na vstupy uživatele.

Takto definovaný `fx:controller` lze chápat jako *presenter* z MVP nebo jako aktivní část *view*, normálně pasivního view deklarovaného v FXML.

`fx:controller` nemůže být plnohodnotným MVC kontrolerem proto, že je přímo svázán s konkrétním FXML a nedokáže tak vyřešit přepínání jednotlivých *view*.

Vnitřní struktura JavaFX s FXML tedy nebrání různým implementacím MVx. Například velmi působivá je implementace Model-View-ViewModel ve frameworku [MvvmFX](http://sialcasa.github.io/mvvmFX).

## Minimální architektonický návrh JavaFX aplikace

Pokud bychom pro návrh architektury pro JavaFX aplikaci s použitím FXML chtěli zvolit co nejjednodušší cestu, vytvoříme:

* byznys třídy reprezentující logiku aplikace,
* spouštěcí třídu, která je potomkem *Application*,
* deklarativní zápis UI ve formě FXML,
* ovladač událostí FXML (`fx:controller`).

V tomto případě je architektura nejblíže MVP. Uživatel interaguje s grafickými ovládacími prvky (*view*), které spouští události v `fx:controller` (*presenter*). Prezentérů může být více, ale mají odkaz na jedno rozhraní byznys tříd (*model*), se kterým mohou manipulovat.

Podstatné je, že jsme **oddělili byznys třídy a třídy UI**. Nezávislost těchto komponent je předpokladem pro dobrou architekturu a vyhovuje širší definici MVC.

## Reakce na události z modelu

Vhodné doplnění takovéto architektury je **návrhový vzor observer**, který vyřeší situace, kdy potřebujeme reagovat na události vznikající v modelu. Pokud bychom návrhový vzor observer nepoužili, nebylo by možné reagovat na události z modelu v UI jiným způsobem než použitím odkazu na třídy UI, což je v rozporu s principem oddělení byznys tříd a tříd UI.

Návrhový vzor observer funguje na principu „odběru novinek“. Ten, kdo je producentem událostí (*observable*), upozorňuje své odběratele (*observer*), že událost nastala. Producentem událostí může být rozhraní byznys tříd nebo jakákoli třída, kterou rozhraní dokáže předat. Odběratelem událostí může být určitý prezentér (`fx:controller`) nebo jiná třída, která je odpovědná za UI.

Návrhový vzor je názorně popsaný v článku [Observer Design Pattern in Java](http://www.codenuclear.com/observer-design-pattern-in-java/).

## Okna aplikace a jejich obsah

Okno aplikace je tvořeno objektem třídy [*Stage*](https://openjfx.io/javadoc/11/javafx.graphics/javafx/stage/Stage.html). Na objektu *Stage* se, podobně jako na každém jiném pódiu, odehrávají různé scény. Objekt třídy [*Scene*](https://openjfx.io/javadoc/11/javafx.graphics/javafx/scene/Scene.html) je vyjádřením konkrétní scény. Scéna potom obsahuje konkrétní JavaFX komponenty (*scene graph*), jako např. GridPane, Button, TextField, HBox a další. Komponenty mají hierarchickou strukturu a všechny dědí z třídy [*Node*](https://openjfx.io/javadoc/11/javafx.graphics/javafx/scene/Node.html), česky uzel. Uzly, které umožňují nést další uzly, jako např. GridPane či HBox, dědí z třídy [*Parent*](https://openjfx.io/javadoc/11/javafx.graphics/javafx/scene/Parent.html). Rodičovský uzel (nebo také kořen či *root*) je na vrcholu hierarchie komponent tvořících obsah scény. Každá komponenta uživatelského rozhraní je v něm obsažena. Kořen je zapotřebí ke konstrukci nové scény.


```java
Parent root = new GridPane();
Scene scene = new Scene(root);
Stage stage = new Stage(scene);
```

![Schéma prvků JavaFX (zdroj: Jenkov.com)](http://tutorials.jenkov.com/images/java-javafx/javafx-overview-1.png){ height=7cm }

### Zobrazení okna s obsahem

Grafické rozhraní JavaFX aplikace se spustí pomocí statické metody `launch()`, kterou obsahuje každý potomek třídy *Application*.

Vhodné místo je například metoda `main()`.

```java
public class Main extends Application {
  public static void main(String[] args) {
    launch(args);
  }
}
```

Kód pro sestavení a zobrazení scény je třeba umístit do metody `start()`, která je povinná u tříd, které dědí z třídy *Application*.

Metoda `start()` má mezi parametry již předem vytvořený objekt třídy *Stage*. Takto předanému objektu se jen nastaví nová scéna.

```java
@Override
public void start(Stage primaryStage) throws Exception {
  Parent root = new GridPane();
  Scene scene = new Scene(root);

  // nastavení nové scény pro primární stage
  primaryStage.setScene(scene);

  //zobrazení okna
  primaryStage.show();
}
```

### Zobrazení FXML

V případě FXML je nutné použít třídu *FXMLLoader*, která dokáže soubor načíst a vrátit odkaz na kořen (*root*). K tomu slouží statická metoda `load(URL location)`. Parametrem metody je objekt třídy *URL*, který směřuje na umístění FXML souboru.

V případě Maven projektu je možné využít standardní lokace pro zdroje a soubor umístit do adresáře `src/main/resources/` voláním `getClass().getResource("/fxmlsoubor.fxml")`.


```java
@Override
public void start(Stage primaryStage) throws Exception {
  // získání URL souboru
  URL fxmlSoubor = getClass().getResource("/fxmlsoubor.fxml");

  // načetení FXML souboru
  Parent root = FXMLLoader.load(fxmlSoubor)

  Scene scene = new Scene(root);
  primaryStage.setScene(scene);
  primaryStage.show();
}
```


### Časté chyby
* FXML soubor není umístěný v předkonfigurovaném (*Maven*) adresáři pro zdroje `src/main/resources/`.
* Chybí lomítko u cesty k FXML souboru. Lomítko označuje, že jde o kořen adresáře určeného pro zdroje (*Maven*).
* V některých případech při použití Java SDK verze 11 je nutné nakonfigurovat správně moduly aplikace v souboru `module-info.java`, viz [založení nového projektu pro Javu verze 11](#zalozeni-D).
* Aplikace se nezobrazí, protože chybí volání metody `show()`.
* FXML obsahuje chyby v syntaxi, a proto soubor nelze načíst. Je dobré vždy otestovat, zda funguje prázdné FXML jen s kořenovým prvkem, např. GridPane.
