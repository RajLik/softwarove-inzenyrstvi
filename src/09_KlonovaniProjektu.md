
# Klonování existujícího projektu {#klonovaniProjektu}

Klonování projektu je postup, při kterém se vytvoří lokální úložiště jako obraz vzdáleného.

Klonování existujícího projektu je o poznání snazší než sdílení projektu. Stejně jako u sdílení je možné použít přímo git, IDE nebo GUI klient obsluhující git (např. *GitKraken*).

Všechny způsoby používají příkaz `git clone`. Ten vždy vytvoří nové lokální úložiště a zrcadlí jeho obsah, včetně historie. Stažený projekt je následně potřeba otevřít v IDE, které načte projektovou konfiguraci z objektového modelu *Maven* (`pom.xml`) nebo jiných konfiguračních souborů, které jsou k tomu účelu k dispozici (*Gradle*, nastavení IDE).

Pro klonování potřebujete znát URL vzdáleného repozitáře. V případě GitLab ho najdete na úvodní stránce projektu pod modrým tlačítkem *Clone*. URL závisí na protokolu přenosu souborů. Je možné vybrat buď *SSH*, nebo *HTTPS*.

Doporučený způsob je *SSH*, vyžaduje ale vygenerování a nahrání veřejného klíče na GitLab.com. Metoda je bezpečnější a při práci se vzdáleným repozitářem není potřeba zadávat heslo. Při zvolení *HTTPS* je heslo nutné, ale většina programů jej umí uložit. Při použití *HTTPS* dejte pozor na uložení špatně zadaného hesla, viz podkapitola [Časté chyby](#casteChybySdileni).

Všimněte si, že kopírovaná adresa repozitáře končí *.git*, na rozdíl od URL stránek projektu, které je až na koncovku stejné.

![Kopírování URL nového projektu na Gitlab.com](images/GitLabCreatedProject.png)

## Klonování pomocí IDE

Klonování pomocí IDE má výhodu, že výše popsaný postup proběhne v jednom kroku.

Na úvodní obrazovce IntelliJ IDEA zvolte položku *Check out from Version Control*. Objeví se dialog *Clone Repository*. Do pole *URL* vložte zkopírované URL. V poli *Directory* se vygeneruje adresář pro umístění projektu. Cestu zkontrolujte a klonování dokončete tlačítkem *Clone*.

![Klonování existujícího projektu v IntelliJ IDEA](images/gitcloneIDEA.png)
