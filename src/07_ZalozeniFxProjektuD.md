
## Maven projekt s JDK 11 a ručním připojením JavaFX – varianta D {#zalozeni-D}

Podmínky pro uplatnění postupu:

* Je nainstalovaná Java OpenJDK verze 11.

Postup je uveden jen pro úplnost. Pokud chcete vyvíjet v SDK 11, je lepší připojit knihovny JavaFX jako Maven závislost ([*varianta B*](#zalozeni-B)).

V případě, že vám podmínky neumožňují vytvořit projekt tímto způsobem, prozkoumejte [další varianty](#zalozeni).

### Postup založení

Postup probíhá stejně jako v případě [*variant A*](#zalozeni-A) a [*C*](#zalozeni-C) s tím rozdílem, že při vytváření použijete **archetyp pro Javu 11**.

Doporučený archetyp `tech.raaf:java11-archetype:1.0.0`. Podrobnosti viz [Maven repozitář](https://mvnrepository.com/artifact/tech.raaf/java11-archetype).

Pokud je to nutné, po založení propojte s nainstalovanou knihovnou na disku jako u [*varianty C*](#zalozeni-C). Knihovna musí být nainstalovaná (viz **kapitola JavaFX**).

Nakonec je třeba **nakonfigurovat modul**. Modul lze konfigurovat jen tehdy, pokud je projekt v IDEA nastaven nejen na SDK 11, ale také jazyková pravidla Javy verze 11. Modul se nachází ve projektovém stromu v `src > main > java` a nese název `module-info.java`. Modul by měl obsahovat minimálně tento kód, přičemž `nazevModulu` představuje plný název hlavního balíčku aplikace (např. `cz.vse.java.xname.nazevfxaplikace`).


```java
module nazevModulu {
    requires javafx.fxml;
    requires javafx.controls;
    exports nazevModulu;
}
```
