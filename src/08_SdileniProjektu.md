
# Sdílení projektu na vzdálený repozitář {#sdileniProjektu}

V předchozí kapitole jsme vytvořili nový JavaFX projekt, jehož obsah je náchylný k nevratným změnám a je dostupný jen lokálně. Každý takový projekt je třeba zahrnout do systému pro správu verzí a propojit se vzdáleným repozitářem. Do vzdáleného repozitáře se projekt jednak zálohuje a jednak je dostupný dalším spolupracovníkům.

Pro dokončení postupu v této kapitole je nezbytné pochopit princip práce se systémem na správu verzí Git, kterému se věnuje kapitola [Správa verzí](#spravaVerzi).

Pro sdílení projektu na vzdálený repozitář je třeba se držet následujícího postupu:

1. inicializace lokálního repozitáře v adresáři projektu,
2. výběr souborů pro zahrnutí do revize a odeslání změn,
2. založení prázdného vzdáleného repozitáře,
3. propojení lokálního a vzdáleného repozitáře,
4. odeslání lokálních změn na vzdálený repozitář.

Pro splnění kroků můžete využít různé nástroje. Kapitola se blíže věnuje použití příkazové řádky, IntelliJ IDEA a GitKraken.

Jako vzdálený repozitář budeme používat *GitLab.com*. GitLab je open source platforma určená pro podporu vývoje aplikací. GitLab, spolu s dalšími alternativami, blíže představila kapitola [Vzdálené repozitáře](#vzdaleneRepozitare).

## Inicializace lokálního repozitáře v adresáři projektu

### Inicializace v příkazové řádce

V příkazové řádce lze inicializaci provést [příkazem `git init`](https://git-scm.com/docs/git-init). Příkaz je potřeba zavolat v kořenovém adresáři projektu.
Inicializaci ověříte zadáním `git status`.

Výpis by měl vypadat takto:

```
On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.idea/
	nazevFxProjektu.iml
	pom.xml
	src/
	target/

nothing added to commit but untracked files present (use "git add" to track)
```

### Inicializace v IntelliJ IDEA

Výběrem z nabídky *VCS > Import into Version Control > Create Git  Repository...* v prostředí IntelliJ IDEA inicializujete lokální repozitář.

![Inicializace lokálního repozitáře](images/shareProjectCreateRepository.png){ height=7cm }

V dialogu vyberte kořenový adresář projektu. Obvykle nese stejný název jako projekt a měl by se v něm na první úrovni nacházet adresář *src*.

![Výběr umístění pro inicializaci repozitáře](images/shareProjectCreateRepositorySelectDirectory.png){ height=7cm }

**Správnou inicializaci** lokálního repozitáře poznáte podle toho, že se v IDE obvykle změní barva názvů souborů v projektovém stromu. V IntelliJ IDEA se v pravém spodním rohu objeví informace o tom, na které jste aktuálně větvi.

![Zobrazení aktuální větve](images/shareProjectCreateRepositoryCompleted.png){ height=4cm }

### Inicializace v GitKraken

*GitKraken* je grafický klient pro správu verzí pomocí systému Git. Odkaz na stažení spolu s dalšími alternativami uvádí kapitola [Instalace Gitu](#instalaceGitu).

V GitKraken se nový repozitář inicializuje volbou *File > Init Repo*.

V dialogu, který se otevře, je potřeba nazvat projekt a zvolit, kde se nachází. Pole *Initialize in* by mělo obsahovat o úroveň vyšší adresář, než je ten kořenový. Kořenový adresář se doplní podle názvu projektu. Před samotným vytvořením si raději zkontrolujte *Full path*.

![Inicializace repozitáře v prostředí GitKraken](images/shareProjectInitializeKraken.png)

Po inicializaci uvidíte základní obrazovku prostředí GitKraken se zobrazením inicializačního commitu. Inicializační commit je prvním záznamem o verzi. Bez něho nejde lokální repozitář v GitKrakenu otevřít.

![Dokončená inicializace repozitáře v prostředí  GitKraken](images/shareProjectGitKrakenInitialized.png)

## Výběr souborů pro zahrnutí do revize a odeslání změn

### Výběr souborů pro správu verzí

V tomto bodě se vyplatí zvážit, které soubory je vhodné sdílet do repozitáře. Obecně platí zásada, že se sdílí **jen zdrojový kód**, případně další zdroje jako např. obrázky nebo HTML.

Vhodné je sdílet nastavení projektu, která jsou **nezávislá na IDE**, např. Maven (*pom.xml*) nebo Gradle, ze kterých se projekt po naklonování snáze nastaví.

Při sdílení projektu **bez Maven nebo Gradle** je třeba zvážit možnosti importu dle každého IDE. Například pro IntelliJ IDEA je třeba držet se rad v [článku oficiální podpory](https://intellij-support.jetbrains.com/hc/en-us/articles/206544839).

Soubory, které nemají být předmětem verzování, je možné nastavit v nástroji na správu verzí (IDE, GitKraken...), případně přímo v souboru `.gitignore` (viz [git-scm.com/docs/gitignore](https://git-scm.com/docs/gitignore)), do kterého se nastavení zapisuje.

Soubor `.gitignore` by se měl nacházet v **kořeni projektu**. Příklad souboru, ve kterém nesledujeme a nesdílíme změny nastavení IDE (adresář `.idea` a soubor s příponou `.iml`) a výstupy kompilace a sestavení aplikace (složka `target`), by mohl vypadat takto:


```
.idea/

*.iml

target/
```

V souvislosti se souborem `.gitignore` bude potřeba vyřešit otázku, zda by měl být soubor také předmětem verzování a sdílen tak dalším vývojářům skrze vzdálený repozitář. Na otázku není jednotný pohled, nicméně vývojáři na [StackOverflow](https://stackoverflow.com/questions/5765645/should-you-commit-gitignore-into-the-git-repos) **doporučují `.gitignore` sdílet**. V případě, že vznikne individuální potřeba pro ignorování dalších souborů, je možné toto upravit v globálním nastavení Gitu.

### Výběr souborů v příkazové řádce {#souboryProVerzovaniPrikaz}

1. V oblíbeném editoru otevřete soubor `.gitignore` umístěný v kořeni projektu nebo jej vytvořte (např. příkazem `nano .gitignore`).
2. Do řádků napište relativní cesty k souborům, které mají být vynechány. Pro zápis lze použít zástupný znak `*` nebo `**` (viz [dokumentace](https://git-scm.com/docs/gitignore)).
3. Po vytvoření `.gitignore` by v projektu měl zůstat k verzování jen zdrojový kód a popřípadě objektový model `pom.xml` a `.gitignore`. Příkaz `git status` by měl vrátit obdobu toho výstupu:


```
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore
	pom.xml
	src/

nothing added to commit but untracked files present (use "git add"
to track)
```

4. Zadáním příkazu `git add -A` se všechny změny převedou do seznamu změn k odeslání (*stage*) a připraví k zápisu. Přepínač `-A` zaznamená všechny nové, změněné nebo smazané soubory. Kromě přepínače `-A` je možné použít `git add -u` pro zaznamenání změn pouze na existujících souborech. Podrobnosti najdete v [dokumentaci příkazu *add* ](https://git-scm.com/docs/git-add).


```
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   .gitignore
	new file:   pom.xml
	new file:   src/main/java/cz/vse/xname/nazevFxProjektu/HomeController.java
	new file:   src/main/java/cz/vse/xname/nazevFxProjektu/Main.java
	new file:   src/main/resources/HomeController.fxml

```

5. Změny odešlete jako revizi příkazem `git commit -m "nahrání souborů"`. Přepínač `-m` není povinný, ale umožní vám zadat název revize v jednom kroku. Bez přepínače `-m` se otevře výchozí textový editor, ve kterém je možné název revize také napsat. Názvy revizí je vhodné volit krátké a výstižné vzhledem k odesílaným změnám.
6. Po úspěšném nahrání zobrazí `git status`, že adresář je beze změn.


```
On branch master
nothing to commit, working directory clean
```


### Výběr souborů v IntelliJ IDEA

V IntelliJ IDEA se změny do revize zahrnou v dialogu s názvem *Commit Changes*, který můžete otevřít z hlavního menu volbou *VCS > Commit...*. Dialog na levé straně zobrazuje seznam nových nebo modifikovaných souborů. Pod seznamem je textová oblast pro zadání názvu změny (*Commit Message*).

![Dialog k tvorbě revize v IntelliJ IDEA](images/commitIDEA.png)

Seznam souborů zahrnuje i ty, které nejsou doporučené pro verzování (nastavení workspace, zkompilované třídy). Proto je potřeba je vyjmout ze sledování verzí.

Vyjmout soubor nebo složku ze sledování můžete po kliknutí pravým tlačítkem na soubor. Zobrazí se dialog *Ignore Unversioned Files*, ve kterém je možné zvolit, zda mají být předmětem vynětí jednotlivé soubory (*specific file*), obsah složky (*files under*) nebo soubory vyhovující masce (*files matching*).

![Vynětí souborů ze sledování změn v IntelliJ IDEA](images/commitIgnoreIDEA.png){ height=4cm }

Takto vyňaté soubory již dále nejsou předmětem sledování změn v IntelliJ IDEA, ale v jiných klientech pro správu verzí mohou být namísto stavu *ignored* ve stavu *unversioned*. Je to způsobené tím, že IntelliJ IDEA **nevytváří** `.gitignore`. Namísto toho soubor, složku nebo masku souborů uloží do nastavení (*.idea/workspace.xml*). K nastavení lze přistoupit otevřením dialogu *File > Settings > Version Control > Ignored Files*.

Z důvodu špatné přenositelnosti seznamu ignorovaných souborů z nastavení IDEA **není toto řešení vhodné** a je lepší vytvořit `.gitignore`, viz kroky 1 a 2 v předchozí kapitole [Výběr souborů v příkazové řádce](#souboryProVerzovaniPrikaz).

Pro odeslání revize je nutné vybrat požadované soubory: zdrojový kód, `.gitignore` a potažmo i Maven konfigurace. Zkontrolujte, zda je vyplněné pole *Author*, a zvolte dostatečně vysvětlující název revize (*Commit Message*). Volby na pravé straně dialogu mohou zůstat nazaškrtnuté. Připravenou revizi odešlete tlačítkem *Commit*.

![Odeslání změn ve formě revize v IntelliJ IDEA](images/commitIDEAselectedFiles.png)

### Výběr souborů v GitKraken

Přidání souborů do správy verzí a ignorování souborů pomocí nástroje GitKraken najdete skvěle zpracované v [návodu na oficiálních stránkách](https://support.gitkraken.com/working-with-commits/staging/).

Nejjednodušší cesta, jak vyjmout soubory ze sledování změn v GitKraken, je volba *Ignore all files in*, která se zobrazí po kliknutí pravím tlačítkem na složku nebo soubor v sekci *Unstaged files*. Vytvoří se tím soubor `.gitignore`, pokud neexistuje, a položka se přidá na nový řádek. Výsledek je stejný jako při ruční konfiguraci souboru `.gitignore` (kroky 1 a 2 v kapitole [Výběr souborů v příkazové řádce](#souboryProVerzovaniPrikaz)).

![Vynětí souborů ze sledování změn v GitKraken](images/ignoreFileGitKraken.png)

Pokud chcete ignorovat soubor, který byl již předmětem sledování změn v minulosti, klikněte pravým tlačítkem na soubor v nabídce *Unstaged Files* a zvolte *Ignore*. Po potvrzení volby kontextového menu se objeví v horní části okna dialog s volbami *Ignore* a *Ignore and Stop Tracking*. Volba *Ignore* vloží záznam do souboru `.gitignore`, ale změny budou stále viditelné. Druhá volba navíc vymaže soubor z indexu repozitáře.

![Dialog k vynětí ze sledování změn v GitKraken](images/gitKrakenIgnoreStopTracking.png)

Zapsání souborů do lokálního repozitáře provedeme následujícím způsobem. Požadované soubory a složky přidáme do *stage* přesunutím z oblasti *Unstaged Files* do *Stage Files*. Přesun lze provést tlačítkem *Stage file*, které se objeví vedle každého souboru, nebo tlačítkem *Stage all changes*, které přesune všechny nové, změněné nebo odebrané soubory (viz přepínač `-A` v dokumentaci).

Po přesunutí souborů do *stage* je možné takto vybrané soubory zaznamenat jako revizi velkým zeleným tlačítkem *Commit changes*. Předtím je ale nutné nastavit jméno revize (*commit message*), které bude dobře vystihovat změnu v projektu.

![Změny připravené k odeslání do lokálního repozitáře v GitKraken](images/GitKrakenCommitAllProjectFiles.png)

## Založení prázdného vzdáleného repozitáře

Před nahráním do vzdáleného repozitáře je třeba založit nový projekt na [GitLab.com](https://gitlab.com) (nebo jiném úložišti dle preference). Nový projekt založíte zeleným tlačítkem *New Project* na úvodní stránce nebo ve vybrané skupině projektů (např. pro vaše cvičení).

Při zakládání vzdáleného repozitáře vždy zvažte, jestli do něj plánujete nasdílet kód z existujícího repozitáře, nebo ho naklonovat jako prázdný a nové soubory do něj vložit (viz podkapitola [Alternativní postup sdílení projektu](#alternativniPostupSdileni)).

Pokud plánujete, v souladu s dosavadním postupem v této kapitole, napojit na vzdálený repozitář již existující lokální repozitář s určitou historií změn, volba ***Initialize repository with a README*** **nesmí být zaškrtnutá!** Způsobilo by to konfliktní historii těchto dvou repozitářů a nebylo by možné je na sebe navázat.

![Založení nového projektu na GitLab.com](images/GitLabCreateProject.png)

## Propojení lokálního a vzdáleného repozitáře

Pro propojení lokálního a vzdáleného repozitáře je nutné zjistit URL vzdáleného repozitáře. Po otevření prázdného projektu na GitLab.com klikněte na modré tlačítko *Clone* a zkopírujte adresu kliknutím na symbol kopie u jednoho ze způsobů klonování *SSH* nebo *HTTPS*.

Podrobnosti o protokolech najdete v kapitole [Klonování existujícího projektu](#klonovaniProjektu).

Při kopírování URL dejte pozor, aby adresa končila na *.git*.

![Kopírování URL nového projektu na Gitlab.com](images/GitLabCreatedProject.png)

### Propojení repozitářů v příkazové řádce

V příkazové řádce přidáte vzdálený repozitář jednoduše příkazem:

`git remote add názevRepozitáře URLrepozitáře`

Jelikož se jedná o první vzdálený repozitář, použijeme obvyklý název `origin`. URL repozitáře zkopírujeme z úvodní stránky našeho projektu na GitLab.com, viz předchozí postup.

Pro SSH například:

`git remote add origin git@gitlab.com:priklad/nazevfxprojektu.git`

Pro HTTPS například:

`git remote add origin https://gitlab.com/priklad/nazevfxprojektu.git`


### Propojení repozitářů v IntelliJ IDEA

V IntelliJ IDEA se napojení na vzdálený repozitář provede v dialogu *Git Remotes*, který se otevře z hlavní nabídky *VCS > Git > Remotes...*.

![Dialog pro přidání vzdáleného repozitáře v IntelliJ IDEA](images/AddRemoteIDEA.png){ height=3cm }

První vzdálený repozitář se přidá stisknutím tlačítka se symbolem plus v pravé horní části dialogu.

Do pole *Name* vyplňte název vzdáleného repozitáře. Jelikož se v tomto případě jedná o první vzdálený repozitář, je zvykem pojmenovat ho *origin*.

Do pole *URL* vložte zkopírovanou adresu vzdáleného repozitáře.

![Napojení na vzdálený repozitář v IntelliJ IDEA](images/addRemoteIDEAstep2.png){ height=3cm }

### Propojení repozitářů v GitKraken

V GitKraken se vzdálený repozitář přidá stisknutím symbolu plus, který se objeví po najetí myši nad položku *REMOTE* v levém panelu.

Po otevření dialogu *Add Remote* zvolte záložku *URL* a vložte zkopírovanou adresu vzdáleného repozitáře do pole *Pull URL* a *Push URL*.

Jelikož se jedná o první vzdálený repozitář, je obvyklé pojmenovat ho *origin* (pole *Name*).

![Napojení na vzdálený repozitář v GitKraken](images/AddRemoteGitKraken.png)

## Odeslání lokálních změn na vzdálený repozitář

Po propojení lokálního a vzdáleného repozitáře je možné odeslat dosavadní zaznamenané revize z lokálního repozitáře na vzdálený příkazem *push*.

V příkazové řádce se *push* provede jednoduše zadáním `git push`.

V GitKraken se příkaz provede kliknutím na tlačítko *Push* v hlavní nástrojové liště v horní části okna.

V IntelliJ se příkaz odesílá z dialogového okna *Push Commits*, které otevřete z hlavní nabídky *VCS > Git... > Push...*

\pagebreak

## Alternativní postup sdílení projektu {#alternativniPostupSdileni}

Alternativně je možné naklonovat prázdný vzdálený repozitář a soubory z nenasdíleného projektu přesunout do nově naklonovaného. Tento postup ovšem přináší následující rizika:

* Projekt bude muset nést jiný název nebo bude muset být vytvořený v jiném umístění na disku, aby nedošlo ke konfliktu v IDE.
* Při manuálním kopírování je riziko, že se nepodaří přenést všechny soubory nebo že budou vznikat konflikty v nastavení projektů.

## Časté chyby při sdílení projektu {#casteChybySdileni}

Při sdílení projektu se můžete setkat s následujícími problémy:

* Při prvním přihlášení na vzdálený repozitář přes HTTPS se může stát, že zadáte špatné heslo. Pokus o připojení ke vzdálenému repozitáři může stále vracet chybu, protože Git klient se na nové heslo i při nezdařených pokusech už nikdy nezeptá. Problém komplikuje také to, že heslo mohlo být uloženo různým způsobem:
  * IntelliJ IDEA ukládá heslo dle nastavení, viz heslo [Passwords](https://www.jetbrains.com/help/idea/reference-ide-settings-password-safe.html) v dokumentaci.
  * Čistý Git používá *credential helper*, který je možné resetovat příkazem `git config --system --unset credential.helper`, viz [gitcredentials](https://git-scm.com/docs/gitcredentials) v dokumentaci.
  * Ve výjimečných případech může být heslo uložené na úrovni operačního systému, např. *Windows Credentials*.
* Inicializace vzdáleného repozitáře při vytváření způsobuje konflikt historie při pokusu o propojení s lokálním repozitářem. Při spojování se ujistěte, že jeden z repozitářů je prázdný.
