
# Java {#java}

*Java* označuje [softwarovou platformu](https://cs.wikipedia.org/wiki/Java_(platforma)) a objektově orientovaný [programovací jazyk](https://cs.wikipedia.org/wiki/Java_(programovac%C3%AD_jazyk)). Java umožňuje spouštět stejný kód na různých operačních systémech a zařízeních pomocí mezivrstvy virtuálního stroje ([Java Virtual Machine](https://cs.wikipedia.org/wiki/Java_Virtual_Machine)).

Kapitola vám pomůže porozumět rozdílu ve verzích, edicích a odlišných licenčních podmínkách implementací Javy. Najdete tu také odkaz na instalační návody a binární soubory Javy.

Pokud byste váhali, zda je Java pro vás to pravé:

* Podle [hackr.io](https://hackr.io/blog/best-programming-languages-to-learn-2019-jobs-future) patří mezi několik **nejlepších voleb** pro budoucí uplatnění kvůli snadnému učení a skvělým pracovním příležitostem.
* V žebříčku popularity [PYPL](https://pypl.github.io/PYPL.html) je na **druhém místě**, za *Pythonem* a před *JavaScriptem*.
* V [průzkumu StackOverflow](https://insights.stackoverflow.com/survey/2019#technology) je na **pátém místě**, po *JavaScriptu*, *HTML*, *SQL* a *Pythonu*. Podle stejného průzkumu nepatří u vývojářů mezi nejmilovanější nebo nejžádanější, ale ani mezi nejstrašnější.
* Na **třetím místě**, po *JavaScriptu* a *Pythonu*, se podle [GitHut](https://madnight.github.io/githut/) drží v objemu aktivit na GitHubu.

*Všechny údaje jsou z léta 2019. Podrobnosti o dalších jazycích najdete např. v [článku na Codinginfinite](https://codinginfinite.com/best-programming-languages-to-learn-2019).*

\pagebreak

## Oracle JDK a OpenJDK

Javu primárně vyvíjí společnost Oracle pod označením *Oracle JDK*.

Oracle JDK s sebou nese **licenční omezení** a umožňuje pouze vývoj, prototypování a vlastní užití. Za produkční nasazení se odvádí poplatky. Podrobné čtení nabízí [plný text licence](https://www.oracle.com/downloads/licenses/javase-license1.html).

Alternativně lze použít svobodnou variantu implementace Javy s otevřeným kódem v podobě projektu [***OpenJDK***](https://openjdk.java.net/).

Implementace OpenJDK je také podporovaná společností Oracle, ale vyvíjí ji komunita pod licencí **GPLv2 s linkovací výjimkou**. Výjimku obsahuje proto, aby bylo možné použít pro svůj program Javu a zároveň pro svůj kód použít libovolnou licenci.

Odkazy na další informace o licenčních podmínkách:

* [plný text licence OpenJDK](http://openjdk.java.net/legal/gplv2+ce.html),
* čtení o [linkovací výjimce](https://cs.wikipedia.org/wiki/GPL_linking_exception),
* čtení o [GPL](https://cs.wikipedia.org/wiki/GNU_General_Public_License),
* situaci okolo licencí Javy skvěle mapuje komunitou vyvíjený dokument [Java Is Still Free](https://docs.google.com/document/d/1nFGazvrCvHMZJgFstlbzoHjpAVwv5DEdnaBr_5pKuHo).

## Verze

Podobně jako například Linux rozlišuje Java mezi stabilními verzemi s dlouhou podporou (*LTS*) a standardními edicemi. Více informací v článku [Long-term support](https://en.wikipedia.org/wiki/Long-term_support) na Wikipedii.

V době vydání skript jsou k dispozici dvě LTS verze – 8 a 11.

Podle průzkumu mezi vývojáři ([2018](https://snyk.io/blog/jvm-ecosystem-report-2018/) a [2019](https://www.jetbrains.com/lp/devecosystem-2019/java/)) byla pro produkční prostředí stále preferovaná **verze 8**. Jelikož byla verze 11 vydána v září 2018, dá se předpokládat, že bude její využití teprve pomalu narůstat.

Rozdíly mezi jednotlivými verzemi jsou dobře popsány v článku [Java version history](https://en.wikipedia.org/wiki/Java_version_history) na Wikipedii a do verze 11 shrnuty v dotazu [Summary of differences between Java versions?](https://softwareengineering.stackexchange.com/a/193652) na Stackexchange.

## Instalace

Javu lze instalovat buď jako prostředí pro běh programů psaných v Javě (dříve JRE, nyní bez označení), nebo spolu s kompilátorem a nástroji pro vývoj (JDK). V rámci vývoje je tedy logické, že budeme používat JDK.

**Stažení a instalace JDK**

* Oracle JDK je ke stažení na [oracle.com](https://www.oracle.com/technetwork/java/javase/downloads).
* OpenJDK:
  * návod pro Linux najdete na [openjdk.java.net](http://openjdk.java.net/install/),
  * binární soubory OpenJDK jsou ke stažení na [adoptopenjdk.net](https://adoptopenjdk.net/).

Instalaci je možné ověřit příkazem `java -version`.

Ujistěte se, že máte nastavenou proměnnou `JAVA_HOME` a že odkazuje na požadovanou instalaci Javy. Návod na její nastavení je například v článku [Set JAVA_HOME on Windows 7, 8, 10, Mac OS X, Linux](http://www.baeldung.com/java-home-on-windows-7-8-10-mac-os-x-linux).

Pokud máte nainstalováno více verzí Javy, je možné mezi nimi přepínat. Na Linuxu například pomocí příkazu `update-alternatives --config java`.

## Další edice

Text kapitoly i celá skripta se věnují **Standard Edition (SE)**. Jak již název napovídá, jedná se o standardní edici Javy.

**Enterprise Edition (EE)** je komunitou vyvíjená edice usnadňující tvorbu komplexních podnikových aplikací s důrazem na technologie a principy jako HTML5, REST a WebSocket. Edice je spojená zejména s aplikačním serverem [GlassFish](https://cs.wikipedia.org/wiki/GlassFish). Více informací najdete v knize [*Your First Cup: An Introduction to the Java EE Platform*](https://javaee.github.io/firstcup/toc.html).

## Tutoriály
Oficiální tutoriály k použití grafiky, zvuku a k práci s časovými údaji nebo připojení k databázi najdete na [docs.oracle.com/javase/tutorial/](https://docs.oracle.com/javase/tutorial/).
