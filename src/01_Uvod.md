
# Úvod

Elektronická skripta jsou určena studentům kurzu **4IT115 Softwarové inženýrství** a vyžadují základní znalost programovacího jazyka *Java*.

Skripta přináší informace o nástrojích, technologiích a doporučených postupech pro vývoj desktopových aplikací v jazyce *Java* s pomocí grafické knihovny *JavaFX*.

Skripta jsou především prakticky orientovaná. Provádí čtenáře od vytvoření softwarového projektu pro knihovnu *JavaFX* až po doručení výsledného produktu a vybavují ho vším, co potřebuje vědět pro efektivní týmovou práci.

Skripta nemají za cíl replikovat dokumentaci použitých technologií a nástrojů, nepřináší ani žádné nové poznatky a inovativní postupy. Za cíl naopak mají **přinést čtenáři podstatné informace a umožnit mu orientovat se v technologiích, doporučených postupech a nejčastějších problémech**. Odkazují se proto na další zdroje, které souvisí s prezentovanými tématy, např. na dokumentaci, odborné články, blogové příspěvky, hesla na Wikipedii, odborné knihy dostupné online, výuková videa na YouTube a názory komunity na webu.

Skripta se zaměřují na tato témata:

* založení nového projektu ([kapitola 7](#novyProjekt)),
* sdílení existujícího projektu na vzdálený repozitář ([kapitola 8](#sdileniProjektu)),
* klonování a úprava existujícího projektu ze vzdáleného repozitáře ([kapitola 9](#klonovaniProjektu)),
* volba architektury JavaFX aplikace ([kapitola 10](#architekturaProjektu)),
* sestavení projektu ([kapitola 11](#sestaveniProjektu)),
* týmová práce na projektu ([kapitola 12](#tymovaPrace)),
* vydání hotové aplikace ([kapitola 13](#release)).

\pagebreak

Použité postupy demonstrují vzájemné propojení těchto technologií:

* grafická knihovna *JavaFX* ([kapitola 3](#javafx)),
* vývojové prostředí *IntelliJ IDEA* ([kapitola 4](#vyvojoveProstredi)),
* nástroj na správu verzí *Git* ([kapitola 5](#spravaVerzi)),
* nástroj na správu a sestavení projektu *Maven* ([kapitola 6](#spravaProjektu)),
* vzdálený repozitář a nástroj na podporu týmové práce a automatizaci *GitLab*.

Skripta jsou primárně určená pro čtení na elektronických zařízeních. Obsahují hypertextové odkazy, které obsah rozšiřují a umožňují hlouběji proniknout do studované problematiky. V tištěné formě není možné odkazy použít, z tohoto důvodu není doporučené skripta tisknout.

Živá verze skript je hostovaná na [gitlab.com/FIS-VSE/studijni-materialy/softwarove-inzenyrstvi](https://gitlab.com/FIS-VSE/studijni-materialy/softwarove-inzenyrstvi).
