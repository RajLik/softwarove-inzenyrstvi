
Tato publikace byla vytvořena za podpory projektu Interní rozvojové soutěže
Vysoké školy ekonomické v Praze s názvem Příprava prostředí a výukových materiálů pro podporu předmětu Softwarové inženýrství (IRS/F4/20/2019).

Oponentem skript je doc. Ing. Alena Buchalcevová, Ph.D.

Pokud není uvedeno jinak, jsou autory obrázků ve skriptech autoři skript.
