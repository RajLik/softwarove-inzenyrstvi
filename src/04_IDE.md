
# Vývojové prostředí {#vyvojoveProstredi}

Kapitola se věnuje výhodám použití integrovaného vývojového prostředí, doporučeným nástrojům a odkazům na stažení těchto nástrojů.

[Vývojové prostředí](https://cs.wikipedia.org/wiki/V%C3%BDvojov%C3%A9_prost%C5%99ed%C3%AD ) neboli IDE (*Integrated Development Environment*) je navrženo pro zvýšení efektivity práce vývojáře. Život vám usnadní zejména následující funkce:

* autocomplete (doplňování jmen proměnných, metod, tříd, cest),
* upozorňování na chyby v kódu (a často i návrhy na opravu),
* snadná navigace v projektu a souborech,
* generování kódu (konstruktory, get a set metody),
* [refaktoring](https://cs.wikipedia.org/wiki/Refaktorov%C3%A1n%C3%AD) (bezpečné přejmenování tříd, metod a proměnných),
* snadný přechod na deklaraci (metody, proměnné, třídy),
* snadný přechod nebo zobrazení dokumentace,
* generování dokumentace,
* generování class diagramu,
* integrace [debuggeru](https://cs.wikipedia.org/wiki/Lad%C4%9Bn%C3%AD_(programov%C3%A1n%C3%AD)) (procházení programu),
* správa nastavení projektu,
* automatické sestavení projektu,
* integrace správy verzí,
* spouštění testů.

Můžete se setkat i s názory, že namísto IDE je lepší použít lehčí editor (*Vim*, *VisualStudio Code*, *Atom*) v kombinaci s dalšími nástroji nebo pluginy na sestavení a testování projektu. Pro vývoj v Javě to ale není příliš obvyklé.

Odpovědi z reálného světa můžete prozkoumat na [StackOverflow](https://stackoverflow.com/a/208221).

## Doporučená prostředí

Pro vývoj v Javě jsou v praxi nejpoužívanější tři nástroje:

* IntelliJ IDEA ([jetbrains.com/idea](https://www.jetbrains.com/idea/)),
* Apache NetBeans ([netbeans.apache.org](https://netbeans.apache.org/)),
* Eclipse IDE for Java Developers ([www.eclipse.org](https://www.eclipse.org/downloads/packages/release/2019-06/r/eclipse-ide-java-developers)).

Všechna zmíněná prostředí jsou k dispozici pro Windows, Mac i Linux.

Ačkoli podle **preferencí uživatelů** ([Slant](https://www.slant.co/topics/607/~best-java-ides-or-editors)) je jejich pořadí stejné jako ve výše uvedeném seznamu, vývojáři na svá oblíbená IDE nedají dopustit. Pokud se chcete lépe zorientovat, skvěle poslouží [článek Martina Hellera](https://www.infoworld.com/article/3114167/choosing-your-java-ide.html).

Seznam dalších možných IDE pro Javu najdete v [článku na hackr.io](https://hackr.io/blog/best-java-ides).

## IntelliJ IDEA

IntelliJ IDEA je k dispozici v komunitní edici *Community* nebo profesionální edici *Ultimate*. Ačkoli je pro projekty v rámci předmětu Softwarové inženýrství dostačující komunitní verze, je možné si požádat o studentskou licenci profesionální verze na [jetbrains.com/student](https://www.jetbrains.com/student/).

Pokud si chcete přečíst o rozdílech ve funkcionalitě více, JetBrains připravilo [podrobné srovnání edic](https://www.jetbrains.com/idea/features/editions_comparison_matrix.html).

Edice *Community* je k dispozici pod licencí [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.html) a umožňuje i použití pro komerční projekty. Studentská licence pro *Ultimate* komerční využití neumožňuje.

Nástroj IntelliJ IDEA **stáhnete** na [jetbrains.com/idea/download](https://www.jetbrains.com/idea/download).

Pro Linux je také k dispozici jako Snap (*[Community](https://snapcraft.io/intellij-idea-community)*, *[Ultimate](https://snapcraft.io/intellij-idea-ultimate)*) nebo Flatpack (*[Community](https://flathub.org/apps/details/com.jetbrains.IntelliJ-IDEA-Community)*, *[Ultimate](https://flathub.org/apps/details/com.jetbrains.IntelliJ-IDEA-Ultimate)*).
