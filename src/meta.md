---
author: Filip Vencovský
title: Nástroje pro tvorbu, správu a nasazování aplikací
subtitle: Elektronická skripta
institute: Vysoká škola ekonomická v Praze
lang: cs
documentclass: book
classoption:
  - oneside
papersize: A4
toc: yes
toc-depth: 1
lof: yes
colorlinks: yes
geometry:
  - left=2.5cm
  - right=2.5cm
  - top=3cm
  - bottom=2.5cm
fontsize: 12pt
linestretch: 1.2
monofont: Source Code Pro
---
