
# Správa verzí {#spravaVerzi}

Na správu verzí lze obecně nahlížet jako na činnost vedoucí ke sledování a organizaci postupných změn na určitých artefaktech.

V knize [Pro Git](https://git-scm.com/book/cs/v2/%C3%9Avod-Spr%C3%A1va-verz%C3%AD) se o správě verzí hovoří jako o systému, který zaznamenává změny souboru nebo sady souborů v čase tak, abyste se mohli později k určité verzi vrátit.

Kapitola se zaměřuje na systém pro správu verzí *Git*, který je de facto standardem pro správu verzí při vývoji softwaru.

## Git

[*Git*](https://cs.wikipedia.org/wiki/Git) je open source systém pro **správu verzí** pod licencí GPLv2.

Oficiální stránky projektu najdete na [git-scm.com](https://git-scm.com/).

Hlavní charakteristiky systému Git:

* Patří mezi **[distribuované systémy](https://cs.wikipedia.org/wiki/Distribuovan%C3%A9_verzov%C3%A1n%C3%AD)**. Na rozdíl od centralizovaných systémů (SVN nebo CVS) si drží celý obraz úložiště a jeho historii nejen na serveru, ale také u klientů.
* Umožňuje pracovat s **více než jedním** vzdáleným úložištěm.
* Podporuje **nelineární vývoj**. Můžete tak pracovat ve více různých větvích a následně je spojovat.
* Nefunguje na bázi seznamu přírůstků, ale udržuje si **celé obrazy úložiště** v každé verzi.
* Kontroluje integritu dat pomocí hashování.

**Výhody** jsou shrnuty na oficiálních stránkách v sekci [About](https://git-scm.com/about).

Ačkoli stále existují projekty používající [Subversion (SVN)](https://cs.wikipedia.org/wiki/Apache_Subversion) nebo [Mercurial](https://cs.wikipedia.org/wiki/Mercurial), Git patří v současnosti mezi nejoblíbenější nástroje. Na **srovnání** se můžete podívat např. na [Stackshare](https://stackshare.io/stackups/git-vs-mercurial-vs-svn) či [Quora](https://www.quora.com/Which-is-better-SVN-or-Git) a na statistiky open source projektů na [OpenHub](https://www.openhub.net/repositories/compare). V roce 2016 dokonce vznikla stránka https://svnvsgit.com/, která se snaží bořit mýty, které vznikly z nadšení pro nový systém.

Pro seznámení s Gitem a hlubší porozumění je nejlepší **[kniha Pro Git](https://git-scm.com/book/cs)** a její tři první kapitoly.

Za zhlédnutí stojí **výuková videa** ze série Git Basics o [verzovacích systémech](https://www.youtube.com/watch?v=8oRjP8yj2Wo) (5:58), [systému Git](https://www.youtube.com/watch?v=uhtzxPU7Bz0) (8:15) a [práci s Gitem](https://www.youtube.com/watch?v=wmnSyrRBKTw) (4:27).

## Instalace Gitu {#instalaceGitu}

Pro Windows a Mac je možné **stáhnout** Git z oficiálních stránek v sekci [Downloads](https://git-scm.com/downloads).

Na Linuxu bývá součástí systémových repozitářů pod názvem `Git`.

S Gitem lze pracovat několika způsoby:

* přímo z příkazové řádky,
* přímo z IDE,
* pomocí externího nástroje.

Některá IDE mají v sobě Git integrovaný a není jej třeba instalovat. V případě **IntelliJ IDEA** musíte odkázat na binární soubor Gitu na disku, abyste jej mohli v IDE používat.

Pokud jste na Windows a nemáte právo instalace, můžete na [oficiálních stránkách](https://git-scm.com/downloads) stáhnout přenosnou verzi (*Portable*).

Seznam externích klientů najdete např. v článku na [Hostinger](https://www.hostinger.com/tutorials/best-git-gui-clients/#Cross-platform-Git-Clients). V rámci předmětu je doporučené jako externí nástroj používat **[GitKraken](https://www.gitkraken.com/download)**, který funguje na operačních systémech Windows, Mac i Linux a nabízí dobrý uživatelský zážitek.

## Vzdálené repozitáře {#vzdaleneRepozitare}

Ačkoli je to možné, nemělo by příliš velký smysl Git používat bez vzdáleného úložiště *(repozitář = úložiště)*, obvykle označovaného jako *origin*. Buď je možné jej hostovat na vlastním serveru, nebo použít jako službu. Projekty s otevřeným zdrojovým kódem je většinou možné hostovat na serverech zdarma.

\pagebreak

Mezi **nejoblíbenější služby** patří:

* [GitLab](https://gitlab.com),
* [GitHub](https://github.com/)
* a [Bitbucket](https://bitbucket.org/).

**Srovnání** dle preferencí uživatelů můžete najít na [Slant](https://www.slant.co/topics/153/~best-hosted-version-control-services).

Zmíněné služby kromě prostoru pro verzovaný kód nabízí další **užitečné funkce** pro podporu vývoje jako zejména sledování problémů (*issue tracking*), revize kódu (*code revisions*), řízení žádostí o spojování větví (*merge requests*), správa dokumentace, integrace do dalších nástrojů, řízení přístupu k větvím a celkové řízení přístupových práv nad úložištěm.

**GitLab** jako open source platforma získal na oblíbenosti mezi vývojáři hlavně kvůli zabudování mechanismů kontinuální integrace a nasazení (*Continual Integration* a *Continual Deployment*, zkratka ***CI/CD***). Po odeslání kódu do úložiště se automaticky provede otestování, sestavení projektu a nasazení. Jako vlastníci projektu pak máte jistotu, že sestavení patří ke konkrétní verzi a prošlo automatizovanými testy.

## Práce se systémem Git

Git pracuje se soubory v adresáři jako obvykle. Rozdíl je v tom, že Git v adresáři sleduje, co se změnilo od poslední verze (revize).

Když se rozhodnete změny zapsat jako novou revizi, přidáte změněné soubory podle svého uvážení do **seznamu změn** (*stage*). Seznam změn následně zapíšete jako novou revizi s dobře zvoleným komentářem, abyste se vy i další vývojáři v úložišti dobře vyznali.

Ve skutečnosti se soubory mohou nacházet ve **čtyřech různých stavech**: *untracked*, *modified*, *staged*, *unmodified*/*commited*. Podrobnosti najdete v kapitole [Nahrávání změn do repozitáře](https://git-scm.com/book/cs/v2/Z%C3%A1klady-pr%C3%A1ce-se-syst%C3%A9mem-Git-Nahr%C3%A1v%C3%A1n%C3%AD-zm%C4%9Bn-do-repozit%C3%A1%C5%99e).

Mezi základní funkce Gitu patří:

* zaznamenání změn jako nové revize (`commit`),
* přidání souboru do seznamu změn (`add`),
* informace o stavu úložiště (`status`),
* zobrazení změn oproti aktuálnímu stavu nebo jiné revizi (`diff`),
* inicializace prázdného repozitáře (`init`),
* klonování vzdáleného úložiště (`clone`),
* vytvoření nové větve (`branch`),
* přepnutí na jinou větev nebo revizi (`checkout`),
* uchování neodeslaných změn při přepínání mezi revizemi a větvemi (`stash`).

Podrobné vysvětlení funkcí najdete v [kapitole 2](https://git-scm.com/book/cs/v2/Z%C3%A1klady-pr%C3%A1ce-se-syst%C3%A9mem-Git-Z%C3%ADsk%C3%A1n%C3%AD-repozit%C3%A1%C5%99e-Git) knihy Pro Git. Pro rychlou orientaci poslouží [tahák](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf).

Na YouTube uvidíte nekomentovaný příklad práce s Gitem v [příkazové řádce](https://www.youtube.com/watch?v=mAWfv8mFR48&t=1s) (10:18) a v [GitKraken](https://www.youtube.com/watch?v=0ZORDwx8rkc) (14:07), který s komentářem naplno zažijete na přednášce.

Tutoriál pro práci v **NetBeans** najdete v článku [Using Git Support in NetBeans IDE](https://netbeans.org/kb/docs/ide/git.html). Práce s Gitem v **IntelliJ IDEA** je demonstrovaná na praktické ukázce v dalších kapitolách.

## Git workflow

Klíčovým faktorem úspěšnosti při použití Gitu, zejména při spolupráci týmu vývojářů, je stanovení způsobu práce s větvemi (*Git workflow*).

V principu je doporučené chránit **hlavní větev (*master*)** proti zápisu běžných změn a ponechat ji jen pro větší fungující celky. Běžné změny se potom odehrávají ve **vývojové větvi**, označované obvykle jako *dev* nebo *develop*. Je na zvážení každého týmu, kdy se uzavře onen větší fungující celek a dojde k připojení vývojové větve na hlavní.

Poměrně běžné je vytváření dalších větví při vývoji nové funkce, opravy chyby nebo ladění hotové verze programu.

Strategie, které je možné zvolit, jsou názorně popsané v **[tutoriálu](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)** nebo článku [Feature branching your way to greatness](https://www.atlassian.com/agile/software-development/branching) od Atlassian.
