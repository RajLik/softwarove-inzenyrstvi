
# Nový JavaFX projekt {#novyProjekt}

Kapitola popisuje postup vytvoření nového projektu založeného na grafické knihovně JavaFX a vývojovém prostředí IntelliJ IDEA.

## Varianty založení nového JavaFX projektu {#zalozeni}

Nový JavaFX projekt lze vytvořit několika způsoby:

* [varianta A](#zalozeni-A): jako Maven projekt s Oracle JDK 8,
* [varianta B](#zalozeni-B): jako Maven projekt z JavaFX archetypu s JDK 11,
* [varianta C](#zalozeni-C): jako Maven projekt s OpenJDK 8 a ručním napojením na JavaFX,
* [varianta D](#zalozeni-D): jako Maven projekt s JDK 11 a ručním napojením na JavaFX,
* [varianta E](#zalozeni-E): jako JavaFX projekt.

Při vytváření projektu **z archetypu JavaFX** (varianta B) je výhodou, že se nemusíte starat o instalaci JavaFX (viz kapitola JavaFX), která se sama stáhne z Maven repozitáře a bude ihned k použití v projektu. Z důvodu nedostupných repozitářů pro JavaFX verze 8 je tento postup možný **jen pro Javu 11** a vyšší verze.

Při vytváření JavaFX projektu **pomocí průvodce** (varianta E) sice projekt vytvoříte, ale pokud nemáte Javu ve verzi 8 na Windows (tj. máte vyšší verzi nebo jiný OS), budete muset do projektu ručně přidat archiv knihovny JavaFX (stejně jako u variant C a D). Navíc bude projekt obtížné sestavovat na počítačích s jiným IDE z důvodu chybějící konfigurace.

Ve všech ostatních případech (varianty A, C a D) se nevytvoří ukázkové soubory JavaFX, ale s knihovnou bude možné pracovat, protože v projektu bude existovat jako Maven závislost. Stejně tak můžete knihovnu JavaFX jako závislost použít v každém již existujícím projektu.

Pro lepší pochopení možností při zakládání nového JavaFX projektu si prohlédněte následující schéma:

![Schéma zakládání projektu](images/schemaZalozeni.png)

Na stránkách nápovědy k IntelliJ IDEA najdete také návod na [vytvoření Maven projektu](https://www.jetbrains.com/help/idea/maven-support.html) (hodí se pro varianty A, B, C, D).
