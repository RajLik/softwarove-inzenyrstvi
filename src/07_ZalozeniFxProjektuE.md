
## IDEA projekt pomocí průvodce – varianta E {#zalozeni-E}

Postup platí pro použití s Java SDK 8 i 11. Využívá průvodce IntelliJ IDEA pro nový JavaFX projekt.

Připojení JavaFX:

* V případě Java OpenJDK 8 nebo obou verzí 11 může být nutné provést ruční propojení s knihovnou JavaFX.
* Po případném převedení na Maven projekt je možné u SDK 11 připojit knihovnu JavaFX jako závislost.

Pokud projekt není později převeden na Maven, je veškeré nastavení projektu uložené jen v souborech IDE, což může způsobit problémy při sdílení projektu dalším osobám.

V případě, že vám podmínky neumožňují vytvořit projekt tímto způsobem, prozkoumejte [další varianty](#zalozeni).

### Postup založení

1. Na úvodní obrazovce zvolit *Create New Project*.

![Úvodní obrazovka IntelliJ](images/ideaWelcomeScreen.png)

2. V seznamu nalevo vybrat *JavaFX*.
3. Vybrat požadovanou verzi **Java SDK**, ve které chcete projekt vytvářet.
4. Kliknout na tlačítko *Next*.

![Průvodce novou JavaFX aplikací](images/ideaNewFxApplication.png)

5. Pojmenovat projekt a případně upřesnit jeho umístění.

![Zadání jména a umístění projektu](images/ideaNewFxApplicationName.png)

6. Projekt se vytvořil se vzorovým balíčkem *sample*, spouštěcí třídou *Main*, prázdným kontrolerem a prázdným FXML.

![Vytvořená JavaFX aplikace](images/ideaNewFxApplicationPoVytvoreni.png)

7. Aplikaci spustit pomocí kliknutí pravým tlačítkem na třídu *Main*, která obsahuje metodu *main*.
8. Z kontextového menu vybrat volbu *Run 'Main'*.

![Spuštění aplikace přes kontextové menu](images/ideaFxApplicationRunContextMenu.png){ height=8cm }

9. Výsledkem by mělo být prázdné okno s názvem *Hello World*.

![Spuštěná aplikace](images/ideaFxApplicationRunOutput.png){ height=7cm }

V případě, že projekt nelze spustit, není vhodně nainstalovaná JavaFX nebo vyžaduje propojení na projekt jako externí knihovna (viz varianta C a D). Pokud používáte Javu 11, je možné použít knihovnu JavaFX jako závislost po převedení na Maven projekt.

### Převedení na Maven projekt

Pokud chcete, aby se projekt dal dobře sdílet jiným vývojářům, je doporučené převést ho na Maven nebo Gradle projekt.

V IntelliJ IDEA je možné každý projekt převést i později na Maven projekt následujícím postupem:

1. Pravým tlačítkem klikněte na název projektu v panelu se strukturou projektu.
2. V kontextovém menu vyberte *Add Framework Support...*

![Přidat podporu frameworku pro projekt](images/addFrameworkSupport.png){ height=6cm }

3. V seznamu frameworků zaškrtněte *Maven*.

![Přidat podporu pro Maven](images/addFrameworkSupportSelectedMaven.png)

4. Projekt se převedl na Maven projekt a vytvořil se objektový model POM.
5. Pozornost věnujte nastavení **importu** a zapněte volbu *Enable Auto-Import*.

![Převedený projekt](images/addFrameworkSupportCreatedPom.png)

5. V objektovém modelu upravte identifikační údaje. *ArtifactId* se přenesl z nastavení IDEA. Stačí tak vyplnit *groupId* na `cz.vse.java.xname`.
6. Doplňte objektový model o obvyklé nastavení pluginů a závislostí (viz ostatní varianty).
7. Po převedení projekt nemusí jít spustit z důvodu jiné struktury složek, než odpovídá konvencím Maven projektu. Pro nápravu je potřeba buď nakonfigurovat objektový model na stávající strukturu, nebo strukturu změnit, aby odpovídala konvencím (viz **kapitola o Maven**).
