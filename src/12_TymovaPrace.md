
# Týmová práce při vývoji softwaru {#tymovaPrace}

Vývoj softwaru v praxi nebývá individuální záležitost. Práce v týmu klade vysoké nároky na koordinaci práce vývojářů. Podcenění těchto aktivit může mít velký dopad na kvalitu nebo dobu trvání projektu. Navíc se v současné době stále častěji prosazuje tendence dodávat software v krátkých intervalech.

Organizaci práce na projektu a koordinaci týmu se věnuje mnoho [metodik vývoje softwaru](https://cs.wikipedia.org/wiki/Metodika_v%C3%BDvoje_softwaru). Příkladem metodik jsou [Scum](https://www.scrumguides.org/), [Kanban](https://www.atlassian.com/agile/kanban), OpenUp a z něho odvozená MMSP. Podrobnosti o metodikách jsou popsány v knize

> BUCHALCEVOVÁ, Alena. Zlepšování procesů při budování informačních systémů. Vydání první. Praha: Oeconomica, nakladatelství VŠE, 2018. 227 stran. ISBN 978-80-245-2235-7.

Integrovaná vývojová prostředí (IDE) pro aktivity vývoje v týmu nabízí jen minimální podporu. Následující seznam je příkladem typů nástrojů, které určitou míru podpory týmového vývoje nabízí:

* issue tracking,
* kanban boards,
* větvení při správě verzí,
* kontinuální integrace a nasazení (*CI/CD*),
* komunikační nástroje.

Kapitola popisuje nástroje, které podporují týmovou práci a jsou dostupné na platformě GitLab.

## Issue tracking

Termín *issue* pokrývá v kontextu vývoje softwaru širokou řadu užití. Může se jednat o

* **otázky** a **problémy**, které je potřeba během vývoje řešit;
* **chyby** v aplikaci identifikované v provozu nebo při testování;
* **požadavky** na vývoj produktu;
* nové **nápady** k diskuzi.

Každý issue má svého původce, osobu, která např. identifikuje chybu nebo nový nápad. Původce zakládá issue do systému GitLab vložením jeho **názvu** a **popisu**.

![Seznam issues na GitLab.com](images/issueListGitLab.png)

K issue je následně potřeba přiřadit **zodpovědnou osobu**. Pravidla pro určování zodpovědných osob je třeba určit před začátkem projektu. Zodpovědnou osobu může určit např. vedoucí týmu nebo se osoba může přiřadit k issue sama. Se zodpovědnou osobou je vhodné vyplnit i **termín pro splnění**.

Každý issue může nést informaci o své váze. **Váha** issue představuje jeho obtížnost, míru úsilí, které je třeba vynaložit pro jeho uzavření.

Některé issues má smysl **přiřazovat k milníkům** projektu. Je tak možné sledovat postup práce na řešení milníku a lépe předejít hrozícímu zpoždění.

Jelikož práce s issues ukládá veškerou **historii**, je možné sledovat to, jak dlouho v minulosti trvalo řešení podobných problémů, nebo vytíženost členů vývojového týmu.

Tím, že issues mají širokou škálu užití, je vhodné je od sebe odlišit. K tomu účelu se používají **štítky** (*labels*). Obvyklé typy issues jsou například:

* User Story,
* Feature,
* Engeneering,
* Bug,
* Question,
* Refactor,
* Documentation,
* Suggestion,
* Enhancement.

Štítky je navíc možné použít k popisu vlastností issues, např.:

* rozpracovanost (To Do, Doing, Done),
* priorita (High, Medium, Low),
* komponenta (Front End, Back End, ...),
* potřebné dovednosti (UX, DB, ...).

Z uvedených příkladů je zřejmé, že štítky jsou silným nástrojem pro týmový vývoj, který dostává smysl hlavně ve vazbě na zvolenou metodiku vývoje.

Podrobnosti můžete najít na stránce [Issues](https://docs.gitlab.com/ee/user/project/issues/) a [Lables](https://docs.gitlab.com/ee/user/project/labels.html) v GitLab dokumentaci.

## Workflow projektu

Workflow by mělo vycházet ze zvolené metodiky a představuje **postup provádění úkonů** v rámci **životního cyklu** softwaru. Ideálně tak zahrnuje práci s

* nápady,
* požadavky a *user stories*,
* vlastním vývojem,
* testováním a
* zpětnou vazbou z provozu aplikace.

Workflow se odráží v práci s issues, zejména za pomoci *kanban board* a štítků, které pomohou udržet informaci o tom, ve které fázi životního cyklu se issue právě nachází.

Velmi doporučené čtení o nastavení workflow můžete najít u [Atlassian](https://www.atlassian.com/agile/project-management/workflow) a podrobného [průvodce workflow](https://about.gitlab.com/blog/2016/10/25/gitlab-workflow-an-overview/) v blogovém příspěvku GitLab.

Příklad workflow z [reálného projektu](https://gitlab.crownplatform.com/crown/crown-core/-/boards/4) obsahuje tyto fáze:

* nový návrh / problém,
* diskuze,
* připraveno pro vývoj,
* ve vývoji,
* revize kódu (peer review),
* schválení,
* slepé konce,
* odloženo (čeká se na určité podmínky),
* bug nelze zreprodukovat.

### Kanban board

Kanban board (také *kanban tabule*) je nástroj, který se obvykle používá v řízení workflow agilního softwarového projektu. Je součástí filozofie štíhlé výroby, konkrétně přístupu [Kanban](https://cs.wikipedia.org/wiki/Kanban).

Principem kanban tabule je vizualizace jednotlivých úkolů (*issues*), která napomáhá **zpřehlednit stav projektu** a zejména **snížit množství současně vykonávaných činností**. K tomu účelu je obvyklé stanovit limit počtu rozpracovaných činností. Limit rozpracovaných činností má smysl i vzhledem k náročnosti spojování více verzí zdrojového kódu do jednoho funkčního celku.

Využití tabule jednak zpřehlední informace o stavu projektu a jednak zjednoduší práci se štítky, protože štítky určitého issue se při jeho přesunutí do jiného sloupce změní samy.

![Kanban board na GitLab.com](images/issuesKanbanBoardGitLab.png)

V praxi může mít projekt více než jednu tabuli. Pokud by tabule měla být jen jedna, bude nejvhodnější použít **fáze zvoleného workflow**. Další tabule se mohou zaměřit na:

* milníky projektu,
* přiřazené osoby,
* priority
* či další dle užitých štítků (dovednosti, komponenty aj.).

Kromě integrované tabule na GitLab.com je možné použít [Trello](https://www.trello.com/) nebo profesionální nástroj [Jira](https://www.atlassian.com/software/jira).

O principu kanban tabule, výhodách fyzických tabulí a rozdílech mezi kanban a scrum tabulí si můžete přečíst v článku [What is a kanban board?](https://www.atlassian.com/agile/kanban/boards) od Atlassian.

Podrobný návod pro použití kanban tabulí na GitLab.com najdete v dokumentaci v článku [Issue Boards](https://docs.gitlab.com/ee/user/project/issue_board.html).

## Práce s git větvemi

Práce s issues umožňuje i systematičtější práci s větvemi během vývoje. Každý issue totiž může být řešený ve vlastní větvi a po otestování se může spojit zpět s vývojovou větví.

GitLab tuto praktiku podporuje tím, že umožňuje pro každý issue založit *Merge request*. Ten se zakládá kliknutím na zelené tlačítko *Create merge request*. GitLab automaticky předvyplní název nové větve podle čísla a názvu issue.

Doporučené je do názvu vložit také prefix podle typu issue, aby bylo jasné, jestli se jedná o opravu chyby (`fix`, `bugfix`) nebo vývoj nové funkcionality (`feature`, `story`). Na základě issue může být vytvořena například větev s názvem `feature/6-herni-plan-s-polohou` nebo `fix/10-nefunkcni-chozeni-mezi-prostory`. Jmenné konvence a zdrojová větev závisí na zvolené strategii pro *git workflow* (viz [přehled strategií](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) pro práci s větvemi od Atlassian).

![Merge request na základě issue](images/issueCreateMergeRequestGitLab.png)

Nově vytvořený merge request na GitLab nese označení *WIP*, které naznačuje, že práce stále probíhá a kód není připravený na testování a spojení s vývojovou větví.

![Status probíhající práce na issue](images/issueMergeRequestWIP.png)

Stejně jako issue by měl každý request mít jednu osobu zodpovědnou za jeho splnění. Nadto je možné přidat podmínku, že spojení musí schválit konkrétní osoby nebo jen určitý počet osob. Tyto osoby jsou spoluodpovědné za otestování funkčnosti a stability změn v kódu.

Teprve ve chvíli, kdy je kód hotový, je možné kliknout na tlačítko *Resolve WIP status*. Tím se z názvu odebere prefix *WIP* a request bude možné spojit.

## Milníky

Milníky (*milestones*) jsou pevné body v čase, které se stanovují v dostatečném předstihu. Definice milníku silně závisí na zvolené metodice. Milník může představovat naplánovaný sprint nebo verzi aplikace.

![Přehled milníků projektu (zdroj: GitLab.com)](https://docs.gitlab.com/ee/user/project/milestones/img/milestones_new_group_milestone.png)

Milník může nést **informace o funkcionalitě nebo opravách**, které vlastník plánuje do aplikace zavést. Tyto informace se vkládají do popisu milníku.

S milníkem je **svázáno více konkrétních issues**, které je třeba před uplnynutím termínu dokončit.

Obvyklou technikou vizualizace vztahu issues k milníku je **burndown chart**. Vizualizace obsahuje časovou osu *x* a počet (nebo celkovou váhu) neuzavřených issues na ose *y*. Od data zahájení a počtu všech přiřazených issues vede orientační přímka k datu konce projektu, oproti které je možné porovnat aktuální stav.

![Burdown chart (zdroj: GitLab.com)](https://about.gitlab.com/images/9_1/burndown_chart.png)

Milníky je možné stanovovat a sledovat na úrovni projektu, ale i skupin projektů.

Více informací o práci s milníky se dozvíte na stránce [Milestones](https://docs.gitlab.com/ee/user/project/milestones/) v dokumentaci GitLab.

## Týmová komunikace a integrace s nástroji na správu issues

Není třeba zdůrazňovat nutnost každodenní týmové komunikace. Na trhu existuje řada nástrojů, které komunikaci vývojářského týmu podporují. Práci na projektu může zefektivnit následující funkcionalita:

* Nezbytností je vyměňovat si části kódu. Nástroje proto umožňují **odesílat zdrojový kód** včetně zvýraznění syntaxe a možnosti ho jednoduše zkopírovat.

* Některé nástroje umožňují **integraci s nástroji na správu issues**. Přímo z konverzace je tak možné **zakládat nové issues** a volat další funkce. Propojení s aplikacemi *GitLab*, *Jira* nebo *Trello* nabízí například [Slack](https://slack.com/apps). Více o propojení mezi aplikacemi GitLab a Slack se dozvíte z dokumentace na stránce [Slack slash commands](https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html).

## Příklady správy projektu z praxe

Příklady správy projektu z open source projektů na GitLab.com najdete v následujícím seznamu. U každého projektu věnujte pozornost použitým issues, štítkům, kanban tabulím, milníkům a strategii pro větvení.

* [Chrome rozšíření pro Trello](https://gitlab.com/boardsummary/BoardSummaryForTrelloChromeExtension)
* [Blockchain platforma](https://gitlab.crownplatform.com/crown/crown-core)
* [Generátor sylogismů](https://gitlab.com/momosa/categorical_syllogisms)
* [Systém pro bezpečné přihlašování a připojení](https://gitlab.com/commonground/nlx/nlx)
* [Galerie obrázků pro Android](https://gitlab.com/HoraApps/LeafPic)
