
# JavaFX {#javafx}

*JavaFX*, nebo také *OpenJFX*, je open source grafická knihovna pro **desktopové aplikace** psané v Javě. Za vývojem stojí komunita a podporu zajišťuje společnost Gluon. Stejně jako OpenJDK je knihovna pod licencí **GPLv2 s linkovací výjimkou**. Ačkoliv knihovnu lze použít i pro vývoj mobilních aplikací, není to v praxi obvyklé.

Oficiální stránku knihovny najdete na [openjfx.io](https://openjfx.io/).

Kapitola vám pomůže orientovat se v možnostech přípravy knihovny k použití ve vývoji aplikace a jiných existujících knihovnách pro vývoj desktopových aplikací na platformě Java.

## Příprava JavaFX k použití

V Oracle JDK 8 je JavaFX ještě součástí standardního balíčku. Od verze 11 a také v případě OpenJDK je třeba JavaFX **nainstalovat zvlášť**.

Při instalaci je třeba věnovat pozornost **verzi knihovny JavaFX**. Ta by měla být shodná s verzí Javy, kterou se rozhodnete používat pro projekt.

### Instalace na disk

Binární soubory příslušné verze JavaFX SDK je možné stáhnout na [gluonhq.com/products/javafx](https://gluonhq.com/products/javafx/).

Po stažení a rozbalení archivu je třeba odkázat na složku s JavaFX proměnnou `PATH_TO_FX`. Ta se nastaví obdobně jako proměnná `JAVA_HOME` popsaná v předchozí kapitole. Návod na instalaci najdete na oficiálních stránkách [openjfx.io/openjfx-docs](https://openjfx.io/openjfx-docs/#install-javafx).

Pokud z nějakého důvodu potřebujete pracovat s OpenJDK 8, musíte se poohlédnout po adekvátní verzi JavaFX 8:

* Ubuntu 16.04 LTS disponuje ve standardním balíčku *openjfx*, v 18.04 LTS také, ale s vynucením verze `sudo apt install *openjfx*=8*`.
* V případě Windows jsou binární soubory dostupné na GitHub.com v projektu [openjfx-win](https://github.com/scoop-software/openjfx-win/releases).
* V případě jiných verzí Linuxu nebo Macu je možné stáhnout JDK třetích stran, která obsahují i Javu FX. Příkladem jsou projekty [Liberica OpenJDK](https://bell-sw.com/pages/downloads/) nebo [Zulu OpenJDK](https://www.azul.com/downloads/zulu-community/).

### Použití závislostí (Maven)

Druhou variantou pro používání knihovny JavaFX je provázání pomocí *Maven* závislostí. V případě připojení knihovny jako závislosti není třeba nic předem instalovat.

Použití závislostí je doporučené jen pro projekty s **Java SDK 11 a vyššími verzemi**, protože v Maven repozitáři nejsou k dispozici binární soubory pro verzi 8.

Nástroji Maven se budeme věnovat ve zvláštní kapitole **Správa projektu**. Použití JavaFX v Maven projektu je součástí kapitoly **Založení FX projektu**.

Návod na nastavení projektu s JavaFX pomocí Maven najdete také na oficiálních stránkách [openjfx.io/openjfx-docs](https://openjfx.io/openjfx-docs/#maven).

## Alternativní grafické knihovny

Ačkoli v minulosti existovala řada podobných knihoven, JavaFX nemá pro desktopové aplikace na Java platformě konkurenci. Seznam alternativních knihoven najdete například v článku [Java applications](https://techsore.com/java-applications).

Na internetu se nezřídka objevují srovnání s knihovnami Swing nebo AWT, které jsou předchůdci JavaFX. Ty jsou sice stále součástí Javy 8 a 11, ale v dalších verzích už nebudou podporovány. Příklad srovnání najdete v článku [JavaFX vs Swing](https://www.educba.com/javafx-vs-swing).

## Další zdroje

Dodatečné informace ke knihovně JavaFX si můžete přečíst v [dokumentaci JavaFX SDK 8](https://docs.oracle.com/javase/8/javase-clienttechnologies.htm) nebo na [původní stránce OpenJFX](https://wiki.openjdk.java.net/display/OpenJFX).
