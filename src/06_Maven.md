
# Správa projektu {#spravaProjektu}

Správu projektu tvoří aktivity, které vedou ke snadné údržbě kódu, správě závislostí, výstupních artefaktů a plánu spouštění testů a sestavení aplikace.

O správu a sestavení projektu se dokáže postarat IDE. Nastavení projektu a potřebné knihovny navolíte v konfiguračních dialogových oknech. Pokud ale nechcete, aby byl projekt závislý na konkrétním vývojovém prostředí a jeho verzi, je vhodné použít jeden ze systémů správy a sestavení projektu. Zejména pokud projekt koncipujete jako veřejně dostupný s otevřeným zdrojovým kódem, stává se to de facto standardem. Další důvod pro použití systému na správu a sestavení projektu je kontinuální integrace a nasazení (*CI/CD*), které z nastavení systému mohou čerpat.

Oblíbené nástroje, které se pro tento problém hodí, jsou **[Maven](https://cs.wikipedia.org/wiki/Apache_Maven)** a **[Gradle](https://en.wikipedia.org/wiki/Gradle)**. Jejich porovnání, včetně staršího nástroje pro sestavení *Ant*, shrnuje [článek na Baeldung](https://www.baeldung.com/ant-maven-gradle). Na [Slant](https://www.slant.co/topics/688/~best-java-build-automation-tools) není zatím mnoho hlasujících, ale už tam můžete vidět v bodech základní výhody a nevýhody obou nástrojů.

## Gradle

*Gradle* je široce použitelný nástroj pro automatizaci, který stejně jako Maven řeší i konfiguraci projektu, závislosti a sestavení. Pro konfiguraci používá jazyk [Groovy](http://groovy-lang.org/). Veškeré funkce se zadávají pomocí pluginů a jejich konfigurace.

## Maven

*Maven* je nástroj na správu projektu vyvíjený organizací *Apache*. Oficiální stránku projektu s dokumentací najdete na [maven.apache.org](https://maven.apache.org). Až se budete chtít blíže seznámit s Maven, je vhodné pročíst si [Getting Started Guide](https://maven.apache.org/guides/getting-started/index.html).

Pokud dodržíte předepsané konvence, Maven dokáže vyřešit většinu problémů. V opačném případě jich dokáže naopak spoustu přinést. Konvence například předepisují, jaká má být adresářová struktura projektu, kde se mají hledat zdroje a jak probíhá sestavení.

Pokud by jeho použití mělo mít jen jednu praktickou výhodu, bude to **správa závislostí**. Při vývoji aplikace se nevyhnete použití dalšího kódu. Kód se obvykle stane součástí projektu připojením externích knihoven. Je ale náročné spravovat různé verze externích knihoven a propojovat je vždy s projektem v každém novém prostředí. Maven je napojený na rozsáhlý repozitář [MvnRepository](https://mvnrepository.com/), ve kterém najdete velké množství projektů s uspořádanými vydáními knihoven. Po zapsání krátké deklarace o závislosti na dalších projektech (*dependency*) se kód automaticky stáhne do projektu a můžete ho hned použít.

**Konfigurace projektu** se tvoří na jednom místě v souboru `pom.xml`, který je zkratkou anglického názvu objektového modelu projektu (*Project Object Model*). Jak napovídá přípona, konfigurace se tvoří v jazyce XML. Konvence předepisuje, že soubor by se měl nacházet v kořeni projektu.

O práci na sestavení projektu se starají **pluginy**. Pluginy jsou už předkonfigurované, ale jejich nastavení je možné překrýt vlastním. Seznam a dokumentaci nejběžnějších pluginů najdete na [maven.apache.org/plugins](https://maven.apache.org/plugins/). Například plugin `jar` slouží k zabalení programu do spustitelného archivu. Mimo to můžete použít i pluginy třetích stran, například plugin na sestavení JavaFX projektu od OpenJFX.

Nástrojem, který zefektivňuje práci na vytvoření projektu, jsou předdefinované šablony (**archetypy**). S pomocí archetypu vytvoří Maven strukturu projektových složek a předdefinovaný objektový model. Archetypy mohou obsahovat i ukázkové soubory. Například [*javafx-archetype-simple*](https://mvnrepository.com/artifact/org.openjfx/javafx-archetype-simple) lze použít pro vytvoření jednoduchého JavaFX projektu. Podrobně se s archetypy můžete seznámit v [Úvodu do archetypů](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html) v oficiální dokumentaci.

### Identifikace Maven projektu

Každý Java projekt má třídy organizované do balíčků. Maven projekty dodržují konvenci pro názvy balíčků a jejich hierarchickou strukturu tak, aby **neexistovaly** žádné dvě třídy se stejným jménem a stejnou strukturou balíčků (*classpath*).

To se hodí zejména k tomu, aby bylo možné použít v projektu jakoukoli jinou existující knihovnu a třídu, aniž by došlo ke konfliktu. Je to pochopitelně také jedna z podmínek pro uveřejnění projektu v [oficiálním Maven repozitáři](https://mvnrepository.com/).

Organizace tříd do balíčků začíná v případě Maven projektu už u identifikačních údajů projektu. Když zakládáte nový projekt, musíte vyplnit *GroupId* a *ArtifactId*. Více najdete v dokumentaci projektu v části o [jmenných konvencích](https://maven.apache.org/guides/mini/guide-naming-conventions.html).

Vezměme v úvahu existující příklad systému CHVote pro pořádání voleb ve Švýcarsku, který je veřejně dostupný na [github.com/republique-et-canton-de-geneve/chvote-1-0](https://github.com/republique-et-canton-de-geneve/chvote-1-0). V projektu na GitHub.com je k dispozici i jeho [objektový model (POM)](https://github.com/republique-et-canton-de-geneve/chvote-1-0/blob/master/base-pom/pom.xml).

***GroupId*** by měl představovat unikátní název projektu. Drží se konvencí Javy pro [unikátní názvy balíčků](https://docs.oracle.com/javase/specs/jls/se11/html/jls-6.html#jls-6.1). Není tedy možné používat speciální znaky a názvy musí dodržovat *velbloudí notaci*. *GroupId* je tvořen doménou, kterou organizace vlastní. Pořadí je ale opačné oproti zvyklostem na webu. První je doména prvního řádu, následuje druhý a případně třetí řád. V uvedeném příkladu software produkuje kanton [Genève](https://www.ge.ch/) s webovou adresou *www.ge.ch*. *GroupId* proto bude začínat `ch.ge`. Následuje hierarchická struktura projektů a modulů, které vlastník pod doménu řadí. V uvedeném příkladu je název projektu *ve* (*vote électronique*). *GroupId* je tedy `ch.ge.ve`.

***ArtifactId*** je název produktu (*artefaktu*) v rámci projektu. Pod tímto názvem se potom bude produkt distribuovat. V příkladu `ch.ge.ve` jsou názvy artefaktu `base-pom`, `admin-offline`, `commons-base`.

***Version*** je posledním údajem, který je třeba vyplnit a označuje verzi artefaktu. Pro verzování existují různé zvyklosti. Doporučeným způsobem je sémantické verzování, které se skládá ze tří číslic ve formátu `MAJOR.MINOR.PATCH`. Podrobnosti o sémantickém verzování najdete na [semver.org](https://semver.org/lang/cs/).

Součástí názvu verze bývá typicky příznak ***SNAPSHOT***, např. `1.0.0-SNAPSHOT`, který označuje, že verze je stále ve vývoji a mění se na každodenní bázi. Finální produkty už nesmí být označeny tímto příznakem. Pro více informací si přečtěte vysvětlení komunity na [StackOverflow](https://stackoverflow.com/questions/5901378/what-exactly-is-a-maven-snapshot-and-why-do-we-need-it) nebo [What is a SNAPHOT version?](https://maven.apache.org/guides/getting-started/index.html#What_is_a_SNAPSHOT_version) ze stránek Maven.

Jiným příkladem z open source světa je legislativní databáze státu New York na [github.com/nysenate/OpenLegislation](https://github.com/nysenate/OpenLegislation) s [objektovým modelem (POM)](https://github.com/nysenate/OpenLegislation/blob/dev/pom.xml), ve kterém najdete *GroupId* `gov.nysenate` a *ArtifactId* `legislation`.

Pro projekt v rámci předmětu Softwarové inženýrství použijeme **jednotnou formu *GroupId*** `cz.vse.java.xname`, kde *xname* je váš login do školního systému. *ArtifactId* vyplňte podle požadavků na konkrétní projekt v rámci cvičení.

![Identifikace nového projektu](images/ideaNewMavenArtifact.png){ height=8cm }

### Project Object Model (POM)

Projektový model je místo, ve kterém je uložena konfigurace Maven projektu. Tvoří ho XML soubor s názvem `pom.xml`.

Všechny Maven projekty jsou **potomkem** tzv. [Super POM](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html#Super_POM).

Definice objektového modelu **musí vždy zahrnovat** kořenový element `project`. V něm by měla být verze objektového modelu (element *modelVersion*), ze kterého bude vycházet ten váš. V současnosti se používá verze `4.0.0`. Dále je nutné uvést výše zmíněné identifikační údaje `GroupId`, `ArtifactId` a `version` (viz [Minimal POM](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html#Minimal_POM)).


```xml
<project>
  <modelVersion>4.0.0</modelVersion>
  <GroupId>cz.vse.java.xname</GroupId>
  <ArtifactId>nazevFxProjektu</ArtifactId>
  <version>1.0-SNAPSHOT</version>
</project>
```

Kromě identifikačních údajů je vhodné uvést také další informace o projektu jako `description`, `url`, `licences`, `organization`, `scm`, `issueManagement`, `contributors`, `developers`. Pro bližší informace o všech elementech, které jsou součástí POM, si prohlédněte [dokumentaci Maven](https://maven.apache.org/ref/3.6.1/maven-model/maven.html#project).

**Závislosti** na dalších projektech se tvoří pomocí elementu `dependencies`, do kterého jsou vložené jednotlivé `dependency` s identifikačními údaji závislosti. Závislosti se během sestavování vyhledají a stáhnou z [centrálního repozitáře](https://mvnrepository.com/). Další informace o závislostech jsou v průvodci [How to use  external dependencies](https://maven.apache.org/guides/getting-started/index.html#How_do_I_use_external_dependencies) a [dokumentaci](https://maven.apache.org/ref/3.6.1/maven-model/maven.html#class_dependency).


```xml
<project>

    <!-- jiné části POM -->

    <dependencies>
        <dependency>
            <GroupId>org.junit.jupiter</GroupId>
            <ArtifactId>junit-jupiter-engine</ArtifactId>
            <version>5.4.2</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <!-- jiné části POM -->

</project>
```

### Struktura souborů Maven projektu

Výchozí konfigurace Maven projektu předpokládá, že veškeré soubory projektu budou umístěné v určité struktuře složek. Více informací najdete na webu Maven v části [Introduction to the standard directory layout](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html).

**Objektový model** se nachází v kořeni projektového adresáře.

**Zdrojový kód** se nachází v kořeni projektového adresáře ve složce `src`. Složka se dále dělí na `main` a `test`. Obě složky uvnitř dodržují strukturu, která odpovídá názvu balíčku a identifikačním údajům projektu.

Pokud Váš projekt používá **zdroje** jako například obrázky, zvukové soubory nebo HTML, Maven je bude očekávat ve složce `src/main/resources`.

Složku `class` se **zkompilovanými třídami** najdete v kořeni adresáře ve složce `target`, což je odlišné od jiných zvyklostí, kde narazíte na složky jako *build* nebo *out*.

**Sestavený archiv** najdete taktéž v kořeni adresáře ve složce `target`.

```
nazevFxProjektu
├── pom.xml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── cz
│   │   │       └── vse
│   │   │           └── xname
│   │   │               └── nazevFxProjektu
│   │   │
│   │   └── resources
│   └── test
│       └── java
│           └── cz
│               └── vse
│                   └── xname
│                       └── nazevFxProjektu
└── target
    ├── classes
    └── nazevFxProjektu-1.0-SNAPSHOT.jar

```

Výše uvedené schéma demonstruje strukturu složek typického Maven projektu.
