
# Sestavení projektu {#sestaveniProjektu}

Ať je výsledná forma spustitelný archiv *jar*, webová aplikace *war*, nebo instalovatelný program pro operační systémy Windows, Linux nebo Mac, sestavení aplikace je významná část každého projektu.

Spustitelný archiv *jar* (*Java archive*) je založený na formátu [*zip*](https://en.wikipedia.org/wiki/Zip_(file_format)). Nese zkompilované Java třídy ve struktuře odpovídající balíčkům, zdroje jako obrázky nebo konfigurační soubory a metainformace. Archiv je spustitelný právě díky metainformacím, které obsahují cestu ke spustitelné třídě. Archiv může obsahovat také kontrolu integrity a digitální podpis. Více informací se dozvíte v dokumentaci na stránce [JAR File Specification](https://docs.oracle.com/en/java/javase/11/docs/specs/jar/jar.html).

Kvůli velkému důrazu na automatizaci, který se na vývoj softwaru klade, je optimálním způsobem sestavení takový způsob, který je nezávislý na použitém vývojovém prostředí. Tím se dostáváme k výhodám založení projektu pomocí Maven nebo Gradle, kde je sestavení možné jedním voláním v příkazové řádce.

Maven obsahuje standardně konfiguraci, která dokáže spustitelný archiv sestavit. Za použití pluginů nabízí široké množství přednastavených procedur pro sestavení do různých forem.

Kromě sestavení pomocí Maven se kapitola věnuje také sestavení programem jar a v rámci IDE.

## Maven projekt
Maven projekt je velice jednoduché sestavit. K tomu účelu se používá sled příkazů:

`clean install`

Příkaz **clean** kompletně smaže složku *target*, aby byl projekt sestaven bezpečně bez reziduí z minulých sestavení. Příkaz **install** se postará o vše ostatní: zkompiluje kód, překopíruje zdroje, provede testy a vše zabalí do archivu.

Více informací se dočtete v [Introduction to the Build Lifecycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html) v dokumentaci.

V IDEA můžete jednotlivé příkazy zavolat dvojklikem na příkaz v pohledu přes fáze životního cyklu (*Lifecycle*) v Maven panelu.

![Volání příkazů clean a install](images/buildMavenInstall.png){ height=6cm }

Případně je možné nastavit `clean install` jako spustitelnou konfiguraci. Spustitelná konfigurace se nastavuje volbou *Run > Run...* z hlavního menu. V upřesňujícím dialogu, který se následně otevře, zvolte *Edit Configurations...* Tím se spustí dialog spustitelných konfigurací.

Stisknutím tlačítka se symbolem plus se otevře seznam šablon, mezi kterými je *Maven*. Všechny parametry je možné ponechat a vyplnit jen příkaz (*Command line*) `clean install -f pom.xml`. V příkazu je navíc parametr `-f`, který upřesňuje soubor, ve kterém je projektový objektový model (*pom.xml*).

![Konfigurace volální clean install](images/buildMavenRunConfigurationCleanIstall.png)

To, že součástí sestavení je automatické **provedení jednotkových testů**, je značná výhoda. Při selhání testů se totiž výsledný archiv nevytvoří a nemůže se tak stát, že bude existovat „nefungující“ verze programu.

Plugin pro spustitelný archiv *jar* je už předdefinovaný v archetypu, který používáme. Jediné, na co je třeba si dávat pozor, je **cesta ke spustitelné třídě** a **připojení závislostí**.

Konfigurace pluginu se v objektovém modelu (*POM*) umísťuje vedle ostatních konfigurací pluginů do elementu `<plugins>`, který je součástí `<pluginManagement>` a `<build>` na nejvyšší úrovni.

**Cesta ke spustitelné třídě** je obsahem elementu `<mainClass>` a musí odpovídat plné cestě přes balíčky až po název třídy. Při sestavování se zapíše v archivu do souboru `/META-INF/MANIFEST.MF`.

V situaci, kdy projekt používá **závislosti**, se mohou vyskytnout problémy s přítomností těchto závislostí v archivu. Pro tuto situaci se nabízí dvě řešení:

1. přidat závislosti do spustitelného archivu,
2. zkopírovat závislosti do cílové složky vedle spustitelného archivu.

Pokud to situace umožňuje, je bezpečnější první způsob: vložení závislostí do archivu. Vhodné to nemusí být v případech, kdy to neumožňují licenční pravidla, nebo třeba při větší velikosti připojovaných archivů. Například natrénované modely pro rozpoznávání obrazu nebo textu mohou nabývat velikosti v gigabytech a mohou ztížit manipulaci s programem a jeho aktualizace.

Variantám tvorby archivu se věnuje [článek na Baeldung](https://www.baeldung.com/executable-jar-with-maven).

### Zahrnutí závislostí do archivu

**Závislosti se do spustitelného archivu přidají** za použití [The Apache Maven Assembly Plugin](http://maven.apache.org/plugins/maven-assembly-plugin/) s následující konfigurací v projektovém objektovém modelu (*POM*). Plugin je identifikovaný pomocí *groupId* `org.apache.maven.plugins` a *artefactId* `maven-assembly-plugin`.


```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-assembly-plugin</artifactId>
    <executions>
        <execution>
            <phase>package</phase>
            <goals>
                <goal>single</goal>
            </goals>
            <configuration>
                <archive>
                <manifest>
                    <mainClass>
                        cesta.SpustitelnaTrida
                    </mainClass>
                </manifest>
                </archive>
                <descriptorRefs>
                    <descriptorRef>jar-with-dependencies</descriptorRef>
                </descriptorRefs>
            </configuration>
        </execution>
    </executions>
</plugin>
```


### Ponechání závislostí mimo archiv

Pokud nechceme závislosti zahrnovat do archivu, můžeme použít standardní jar plugin, ale bude nutné závislosti zkopírovat do složky *target*.

Ke kopírování lze použít `maven-dependency-plugin`. Při konfiguraci je nutné věnovat pozornost elementu `<outputDirectory>`, který určuje, do které složky v rámci *target* se mají závislosti překopírovat.


```xml
<!-- standardní jar plugin -->
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-jar-plugin</artifactId>
    <configuration>
        <archive>
            <manifest>
                <addClasspath>true</addClasspath>
                <classpathPrefix>libs/</classpathPrefix>
                <mainClass>
                    cesta.SpustitelnaTrida
                </mainClass>
            </manifest>
        </archive>
    </configuration>
</plugin>

<!-- kopírování závislostí -->
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-dependency-plugin</artifactId>
    <executions>
        <execution>
            <id>copy-dependencies</id>
            <phase>prepare-package</phase>
            <goals>
                <goal>copy-dependencies</goal>
            </goals>
            <configuration>
                <outputDirectory>
                    ${project.build.directory}/libs
                </outputDirectory>
            </configuration>
        </execution>
    </executions>
</plugin>
```


Při ponechání závislostí mimo archiv vzniká **riziko neúplného přenesení aplikace**. Pro snížení rizika je možné aplikaci sestavit jako **instalovatelnou**. K tomu je možné použít např. [plugin IzPack](https://izpack.atlassian.net/wiki/spaces/IZPACK/pages/491576/Compiling+Using+Maven).

## Alternativní způsoby sestavení

### Program jar

Pro sestavení archivu pomocí programu *jar* se používá příkaz definovaný jako

`jar [OPTION...] [ [--release VERSION] [-C dir] files] ...`

Spustitelný archiv se vytvoří například následujícím příkazem. Přepínač `--create` nastaví, že archiv se bude vytvářet, nikoli aktualizovat. Parametr `--file` určí výsledný soubor. Parametr `--main-class` nastaví cestu ke spustitelné třídě. Parametr `-C` stanoví, ve kterém adresáři se nachází soubory, které se mají zabalit. Následuje tečka `.`, která reprezentuje všechny soubory v tomto adresáři.

`jar --create --file target/nazevFxProjektu-1.0.0.jar `

`--main-class cesta.SpustitelnaTrida -C target/classes/ .`

Toto řešení nezahrne do archivu Maven závislosti a před sestavením se neprovedou testy. Proto je doporučené archiv vytvářet pomocí programu *Maven*.

Více informací o programu *jar* najdete v [dokumentaci](https://docs.oracle.com/javase/9/tools/jar.htm).

### Sestavení v IDE

Alternativou k programu *jar* je sestavení archivu pomocí vývojového prostředí. Tím, že projekt ve vývojovém prostředí už obsahuje určitá nastavení, např. závislosti na knihovnách, je proces sestavení uživatelsky méně náročný. Následující příklad popisuje práci na sestavení v IntelliJ IDEA.

Předtím než je možné v IntelliJ IDEA spustit samotné sestavení, je třeba nakonfigurovat výstupní artefakt archivu. To je možné v dialogu *Artifacts*, který je dostupný po zvolení volby *File > Project Structure...*

Artefakt se přidává stiskem tlačítka se symbolem plus a vybráním příslušného typu artefaktu, v našem případě *JAR*.

![Dialog k artefaktům v nastavení projektu](images/buildIdeaArtifacts.png)

Otevře se dialog s nastavením archivu. V něm je třeba vybrat modul projektu (*Module*). V našem případě jednomodulového projektu je jediná možnost, a to zvolit spustitelnou třídu s její plnou cestou (*Main Class*) a vybrat, zda do archivu mají být zahrnuty závislosti.

Adresář *META-INF* je třeba umístit do adresáře *java*, který obsahuje strukturu adresářů odpovídající balíčkům. Toto místo se obvykle zvolí automaticky bez problému.

Poslední volba se týká zahrnutí testů do archivu (*Include tests*). Nejde ovšem o jejich provedení při sestavování jako ověření kvality, které se provádí v případě *Maven install*, ale skutečně o zahrnutí testovacích tříd do archivu. Z toho důvodu můžeme ponechat volbu nezaškrtnutou.

![Dialog přidání nového jar archivu jako artefaktu](images/buildIdeaCreateJar.png){ height=5cm }

Po potvrzení dialogu se, kromě konfiguračních položek v *.idea*, vytvoří v adresáři *src/main/java/META-INF* soubor *MANIFEST.MF*, ve kterém je uložená cesta ke spustitelné třídě.

Archiv se potom sestaví volbou *Build > Build Artifacts...* a výběrem artefaktu a příslušné akce. Možné akce jsou sestavení (*Build*),

![Dialog k sestavení archivu](images/buildIdea.png){ height=3cm }

Tento postup zahrne do výsledného archivu závislosti, ale na rozdíl od sestavení archivu pomocí *Maven* nebo *Gradle* nelze použít pro automatizaci, například v [integračním skriptu na GitLab.com](https://docs.gitlab.com/ee/ci/) nebo [Jenkins serveru](https://jenkins.io/).

Oficiální návod najdete na stránce [Working with Artifacts](https://www.jetbrains.com/help/idea/working-with-artifacts.html).

## Spuštění archivu

Hotový archiv *jar* se spouští nejlépe v **příkazovém řádku** pomocí
`java -jar nzevFxProjektu-1.0-SNAPSHOT.jar`.

Pokud spouštění dělá problémy, ujistěte se, že používáte kompatibilní verzi Java s tou, na kterou byl projekt sestaven, příkazem `java -version`.
