
## Maven projekt s JDK 11 – varianta B {#zalozeni-B}

Podmínky pro uplatnění postupu:

* Je nainstalovaná Java Oracle JDK 11 a OpenJDK 11.

Připojení knihovny JavaFX:

* Jelikož JavaFX není součástí JDK 11, je do projektu připojena jako Maven závislost.

Jedná se o doporučený postup v případě, že není možné nainstalovat Java Oracle JDK verze 8. Pokud vám podmínky neumožňují vytvořit projekt tímto způsobem, prozkoumejte [další varianty](#zalozeni).

### Postup založení projektu

1. Po spuštění IntelliJ IDEA zvolit na úvodní obrazovce *Create New Project*.

![Úvodní obrazovka IntelliJ](images/ideaWelcomeScreen.png){ height=7cm }

\pagebreak

2. V levém menu vybrat *Maven*.
1. Z rozbalovacího seznamu vybrat SDK 11.
1. Vybrat volbu *Create from archetype*.
1. Vybrat archetyp `org.openjfx:javafx-archetype-simple`. Pokud archetyp neexistuje, přidejte ho jako [nový archetyp](#archetypNeexistujeB).

![Vybrat javafx-archetype-simple](images/ideaCreateMavenSelectedArchetype.png)

6. Zvolit SDK Javy, který chcete používat.
1. Potvrdit SDK a archetyp tlačítkem *Next*.
1. Vyplnit identifikační údaje nového Maven projektu (viz kapitola **Správa projektu > Identifikace Maven projektu**).
1. Potvrdit identifikační údaje tlačítkem *Next*.

![Identifikace nového projektu](images/ideaNewMavenArtifact.png){ height=7cm }

### Přidání archetypu, pokud neexistuje {#archetypNeexistujeB}

Pokud se v bodě 4 v seznamu archetypů nenacházel `org.openjfx:javafx-archetype-simple`, je třeba ho přidat jako **nový archetyp** následujícím způsobem:

4a. Vpravo nahoře kliknout na tlačítko *Add Archetype...*

![Přidat archetyp](images/ideaCreateMavenFromArchetype.png)

4b. Vyplnit údaje archetypu `org.openjfx:javafx-archetype-simple` dle Maven [repozitáře](https://mvnrepository.com/artifact/org.openjfx/javafx-archetype-simple).

![Přidat nový archetyp](images/ideaCreateMavenNewArchetype.png){ height=4cm }

### Dokončení postupu

Po potvrzení identifikačních údajů nového Maven projektu (groupId, artifactId, version):

10. Potvrďte výchozí nastavení pro použití Maven (soubor s nastavením a umístění staženého kódu) a vlastností projektu.

![Vlastnosti nového projektu](images/ideaNewProjectProperties.png){ height=7cm }

11. V poslední obrazovce dialogu potvrďte název projektu a jeho umístění na disku.

![Název a umístění projektu](images/ideaNewProjectName.png){ height=7cm }

Projekt se vytvořil a zobrazila se struktura souborů a nově vytvořený objektový model (POM).

![Vytvořený projekt](images/ideaPoVytvoreniProjektu.png)

12. Pozornost věnujte malému informačnímu dialogu vpravo dole, kde je možné spravovat vlastnosti **importu**. Doporučené je kliknout na volbu *Enable Auto-Import*, která zaručí, že knihovny se budou stahovat automaticky při změně objektového modelu. Vlastnosti importu můžete upravovat i později v nastavení v dialogu *Build, Execution, Deployment > Build Tools > Maven > Importing* zaškrtnutím volby *Import Maven projects automatically*.

![Nastavení importu](images/ideaMavenImportSettings.png)

### Spuštění projektu

Projekt se spustí kliknutím na spustitelnou třídu (s metodou *main*) pravým tlačítkem a vybráním volby *Run*.

![Spustitelná třída](images/ideaRunProject.png)

![Kontextové menu třídy](images/ideaRunProjectMenu.png){ height=8cm }

Alternativně je možné vybrat volbu *Run* z kategorie *Run* na horní liště.

Po spuštění se objeví okno s popisem verze JavaFX a JDK.

![Spuštěný projekt](images/JavaFxDefaultContent.png){ height=7cm }

### Možné problémy

Pokud IDEA nerozpoznává názvy tříd knihovny JavaFX, jednou z příčin může být:

* Neproběhlo správně stažení závislostí z Maven repozitáře a je potřeba zavolat ručně *Reimport All Maven Projects*. Volba se nachází v Maven panelu a má symbol dvou zelených šipek v kruhu (refresh symbol).
* Nevytvořila se konfigurace modulu. Vytvoření projektu z archetypu by mělo vytvořit modul a nakonfigurovat ho na použití s JavaFX knihovnou. Pokud se tak nestalo, je třeba řídit se návodem na konfiguraci u [varianty D](#zalozeni-D).
