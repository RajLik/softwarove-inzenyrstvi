
## Maven projekt s Oracle JDK 8 – varianta A {#zalozeni-A}

Podmínky pro uplatnění postupu:

* Je nainstalovaná **Java Oracle JDK verze 8**.

Připojení knihovny JavaFX:

* Jelikož je JavaFX součástí Oracle JDK verze 8, postup záměrně **neřeší její připojení** do projektu.

Jedná se o doporučený postup. V případě, že vám podmínky neumožňují vytvořit projekt tímto způsobem, prozkoumejte [další varianty](#zalozeni).

### Postup založení projektu

1. Po spuštění IntelliJ IDEA zvolit na úvodní obrazovce *Create New Project*.

![Úvodní obrazovka IntelliJ](images/ideaWelcomeScreen.png){ height=7cm }

2. V levém menu vybrat *Maven*.
1. Z rozbalovacího seznamu vybrat SDK 1.8.
1. Zaškrtnout volbu *Create from archetype*.
1. Vybrat archetyp `tech.raaf:java8-archetype`. Pokud archetyp neexistuje, přidejte ho jako [nový archetyp](#archetypNeexistujeA).

![Vybrat java8-archetype](images/ideaCreateMavenSelectedArchetypeJava8.png)

6. Zvolit SDK Javy, který chcete používat. V tomto postupu SDK 8, označený jako *1.8*.
1. Potvrdit SDK a archetyp tlačítkem *Next*.
1. Vyplnit identifikační údaje nového Maven projektu (viz kapitola **Správa projektu > Identifikace Maven projektu**).
1. Potvrdit identifikační údaje tlačítkem *Next*.

![Identifikace nového projektu](images/ideaNewMavenArtifact.png){ height=7cm }

### Přidání archetypu, pokud neexistuje {#archetypNeexistujeA}

Pokud se v bodě 4 v seznamu archetypů nenacházel `tech.raaf:java8-archetype`, je třeba ho přidat jako **nový archetyp** následujícím způsobem:

4a. Vpravo nahoře kliknout na tlačítko *Add Archetype...*

![Přidat archetyp](images/ideaCreateMavenFromArchetypeSdk8.png)

4b. Vyplnit údaje archetypu `tech.raaf:java8-archetype` dle Maven [repozitáře](https://mvnrepository.com/artifact/tech.raaf/java8-archetype).

![Přidat nový archetyp Java 8](images/ideaCreateMavenNewArchetypeJava8.png){ height=4cm }

### Dokončení postupu

Po potvrzení identifikačních údajů nového Maven projektu (groupId, artifactId, version):

10. Potvrďte výchozí nastavení pro použití Maven (soubor s nastavením a umístění staženého kódu) a vlastností projektu tlačítkem *Next*.

![Vlastnosti nového projektu](images/ideaNewProjectPropertiesJava8.png)

11. V poslední obrazovce dialogu potvrďte název projektu a jeho umístění na disku tlačítkem *Finish*.

![Název a umístění projektu](images/ideaNewProjectName.png){ height=7cm }

Projekt se vytvořil a zobrazila se struktura souborů a nově vytvořený objektový model (POM).

![Vytvořený projekt](images/ideaPoVytvoreniProjektuJava8.png)

12. Pozornost věnujte malému informačnímu dialogu vpravo dole, kde je možné spravovat vlastnosti **importu**. Doporučené je kliknout na volbu *Enable Auto-Import*, která zaručí, že knihovny se budou stahovat automaticky při změně objektového modelu. Vlastnosti importu můžete upravovat i později v nastavení v dialogu *Build, Execution, Deployment > Build Tools > Maven > Importing* zaškrtnutím volby *Import Maven projects automatically*.

![Nastavení importu](images/ideaMavenImportSettings.png)


### Spuštění projektu

Projekt se spustí kliknutím na spustitelnou třídu (s metodou *main*) pravým tlačítkem a vybráním volby *Run*.

![Spustitelná třída](images/ideaRunProjectJava8.png)

![Kontextové menu třídy](images/ideaRunProjectMenuHelloWorld.png){ height=8cm }

Alternativně je možné vybrat volbu *Run* z kategorie *Run* na horní liště. V některých případech IDEA nenavrhne třídu ke spuštění a je třeba ji poprvé spustit z kontextové nabídky. Vytvoří se tím konfigurace spuštění. Jinak je možné konfiguraci spuštění přidat také ručně volbou *Run...* v kategorii *Run* a následně vybrat v seznamu na pravé straně *Application*.

Po spuštění se objeví v konzoli ve spodní části obrazovky text *"Hello World"*.

Jak je zřejmé, aplikace zatím nepoužívá JavaFX knihovnu a netvoří žádné okno.
